#ifndef RGI_VERSION_H
#define RGI_VERSION_H

#define MAJOR 0
#define MINOR 1
#define PATCH 1

#define AUTHOR "Babyloon/Revelation (pierrejean@turpeau.net)"

extern void rgi_banner (char *n);

#endif /* RGI_VERSION_H */
