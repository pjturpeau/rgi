#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#ifdef HAVE_CONFIG_H
#include "rgi_config.h"
#endif /* HAVE_CONFIG_H */

#include "rgi_types.h"
#include "rgi_colors.h"
#include "rgi_pixels.h"
#include "rgi_surface.h"

void
rgi_free_surface (rgi_surface *s)
{
    if (s -> pixels)
        free (s -> pixels);
    rgi_free_pixel_format (s -> format);
    free (s);
    s = NULL;
}

rgi_surface *
rgi_new_surface ()
{
    rgi_surface *s;
    s = (rgi_surface *) malloc (sizeof (rgi_surface));
    assert (s);
    return s;
}

rgi_surface *
rgi_create_surface (int width, int height, int bits_per_pixel, 
                    uint32 Rmask, uint32 Gmask, uint32 Bmask, uint32 Amask,
                    uint32 flags)
{
    rgi_surface *s;

    s = rgi_new_surface ();

    s -> flags = flags;
    s -> format = rgi_create_pixel_format (bits_per_pixel,
                                           Rmask, Gmask, Bmask, Amask);
    s -> w = width;
    s -> h = height;
    s -> size = s -> w * s -> h;

    s -> realw = width * s -> format -> BytesPerPixel;
    s -> realh = height;
    s -> realsize = s -> realw * s -> realh;

    s -> pixels = (void *) malloc (s -> realsize);
    assert (s -> pixels);

    return s;
}

void
rgi_printf_surface (rgi_surface *s)
{
    printf ("  width: %d\n", s -> w);
    printf ("  height: %d\n", s -> h);
    printf ("  size: %d\n", s -> size);
    printf ("  real width: %d\n", s -> realw);
    printf ("  real height: %d\n", s -> realw);
    printf ("  real size: %d\n", s -> realsize);
    printf ("  ----FORMAT: %p\n", s -> format);
    rgi_write_pixel_format (s -> format);
    printf ("  -----\npixels: %p\n", s -> pixels);
}

