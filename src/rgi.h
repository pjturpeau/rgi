#ifndef RGI_H
#define RGI_H

#ifdef HAVE_CONFIG_H
#include "rgi_config.h"
#endif /* HAVE_CONFIG_H */

#include "rgi_types.h"
#include "rgi_version.h"
#include "rgi_timer.h"
#include "rgi_colors.h"
#include "rgi_pixels.h"
#include "rgi_surface.h"
#include "rgi_convert.h"
#include "rgi_video.h"

extern char rgi_app_name[STATIC_STRING];
extern char rgi_loop;

extern void rgi_init (int argc, char *argv[], char *app_name);
extern void rgi_set_app_name (char *app_name);
extern void rgi_quit ();
extern void rgi_write_infos (rgi_viewport *v);
extern void rgi_error (const char *err);

#endif /* RGI_H */
