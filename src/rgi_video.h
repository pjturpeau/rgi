#ifndef RGI_VIDEO_H

#ifdef HAVE_CONFIG_H
#include "rgi_config.h"
#endif /* HAVE_CONFIG_H */

#ifndef X_DISPLAY_MISSING
#include "unix/rgi_sys_video.h"
#include "unix/rgi_sys_tools.h"
#endif /* X_DISPLAY_MISSING */

#ifdef DOS_VIDEO
#include "dos/rgi_sys_video.h"
#endif /* DOS_VIDEO */

//efine TMS_X11_VIDEO (1<<0)	/* Pure X11 (with/without shm) */
//efine TMS_DGA_VIDEO (1<<1)	/* Direct Graphic Access */
//efine TMS_DOS_VIDEO (1<<2)	/* MS-DOS */
//efine TMS_SLB_VIDEO (1<<3)	/* SVGA LIB */
//efine TMS_DTX_VIDEO (1<<4)	/* Direct X */
//efine TMS_BOS_VIDEO (1<<5)	/* Be OS */
//efine TMS_MAC_VIDEO (1<<6)	/* MAC OS */

/* video flags */
#define RGI_FULLSCREEN	(1<<0)	/* Use fullscreen if possible */
#define RGI_HIDEPTR	(1<<1)	/* Hide the mouse pointer */
#define RGI_NORESIZE	(1<<2)	/* Disable resize in window mode */
#define RGI_ANYFORMAT	(1<<3)	/* Allow any format for the output */

extern void rgi_init_video ();
extern void rgi_close_video ();

extern rgi_viewport *rgi_set_videomode (int width,
					int height,
					int bpp,
					uint32 flags);

extern void rgi_close_videomode (rgi_viewport *v);

extern void rgi_update_video (rgi_viewport *v);

extern void rgi_free_viewport (rgi_viewport *v);

extern void rgi_write_video_infos (rgi_viewport *v);

extern void rgi_set_palette (rgi_palette *p, rgi_viewport *v);

extern void rgi_set_colors (rgi_viewport *v,
			   rgi_color *c,
			   int start,
			   int end);

#endif /* RGI_VIDEO_H */
