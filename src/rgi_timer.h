#ifndef RGI_TIMER_H
#define RGI_TIMER_H

#define DEFAULT_MAX_FPS 25

extern void  rgi_set_max_fps (int maxfps);
extern void  rgi_init_timer ();
extern void  rgi_update_timer ();
extern void  rgi_end_timer ();
extern float rgi_get_fps ();
extern int   rgi_get_timer_status ();
extern void  rgi_timer_sleep ();

#endif /* RGI_TIMER_H */
