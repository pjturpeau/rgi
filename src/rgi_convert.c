#include <stdlib.h>
#include <stdio.h>

#ifdef HAVE_CONFIG_H
#include "rgi_config.h"
#endif /* HAVE_CONFIG_H */

#include "rgi_types.h"
#include "rgi_colors.h"
#include "rgi_pixels.h"
#include "rgi_surface.h"
#include "rgi_16rgb.h"
#include "rgi_8indexed.h"
#include "rgi_convert.h"

rgi_convert_info rgi_c_functions[] =
{
    {16, 0xF800, 0x7E0, 0x1F, 32, 0xFF0000, 0xFF00, 0xFF, rgi_convert_LE_C_16RGB565_32RGB888},
    {16, 0xF800, 0x7E0, 0x1F, 8, 0, 0, 0, rgi_convert_LE_C_16RGB565_8INDEXED},
  
    {8, 0, 0, 0, 16, 0xF800, 0x7E0, 0x1F, rgi_convert_LE_C_8INDEXED_16RGB565},
    {8, 0, 0, 0, 32, 0xFF0000, 0xFF00, 0xFF, rgi_convert_LE_C_8INDEXED_32RGB888},
  
    {0, 0, 0, 0, 0, 0, 0, 0, NULL}

};

#ifdef __USE_X86_ASSEMBLER__

rgi_convert_info rgi_x86_functions[] =
{

    //   {16, 0xF800, 0x7E0, 0x1F, 32, 0xFF0000, 0xFF00, 0xFF, TMS_Convert_x86_16RGB565_32RGB888},
    //  {16, 0xF800, 0x7E0, 0x1F, 8, 0, 0, 0, TMS_Convert_x86_16RGB565_8INDEXED},
  
    {8, 0, 0, 0, 16, 0xF800, 0x7E0, 0x1F, rgi_convert_x86_8INDEXED_16RGB565},
    {8, 0, 0, 0, 32, 0xFF0000, 0xFF00, 0xFF, rgi_convert_x86_8INDEXED_32RGB888},
    {0, 0, 0, 0, 0, 0, 0, 0, NULL}

};
#endif /* __USE_X86_ASSEMBLER__ */

rgi_convert_info *rgi_asm_functions;

void
rgi_init_convert ()
{
#ifdef __USE_X86_ASSEMBLER__
    rgi_asm_functions = rgi_x86_functions;
#else 
    rgi_asm_functions = NULL;
#endif /* __USE_X86_ASSEMBLER__ */
}

void rgi_dummy (rgi_surface *s_dst, rgi_surface *s_src) {}

static rgi_convert_fun
rgi_sub_get_convert_function (rgi_surface *s_dst, rgi_surface *s_src,
                              rgi_convert_info *funclist)
{
    int i;

    for (i = 0; funclist[i].convert_func != NULL; i++)
        if (s_src -> format -> BitsPerPixel == funclist[i].srcbpp
            && s_src -> format -> Rmask == funclist[i].srcRmask
            && s_src -> format -> Gmask == funclist[i].srcGmask
            && s_src -> format -> Bmask == funclist[i].srcBmask
            && s_dst -> format -> BitsPerPixel == funclist[i].dstbpp
            && s_dst -> format -> Rmask == funclist[i].dstRmask
            && s_dst -> format -> Gmask == funclist[i].dstGmask
            && s_dst -> format -> Bmask == funclist[i].dstBmask)
            return funclist[i].convert_func;

    return NULL;
}

rgi_convert_fun
rgi_get_convert_function (rgi_surface *s_dst, rgi_surface *s_src)
{
    rgi_convert_fun stdfunc;
    rgi_convert_fun asmfunc;

    stdfunc = rgi_sub_get_convert_function (s_dst, s_src, rgi_c_functions);
    if (stdfunc != NULL)
        printf ("\n C convertion function found\n");
    else
        printf ("\n No C convertion function found\n");

    if (rgi_asm_functions != NULL)
    {
        asmfunc = rgi_sub_get_convert_function (s_dst, s_src, rgi_asm_functions);
        if (asmfunc != NULL)
        {
            printf (" ASM convertion function found\n");
            stdfunc = asmfunc;
        }
        else
            printf (" No ASM convertion function found\n");
    }

    if (stdfunc == NULL)
    {
        printf (" using GENERIC function\n");
        stdfunc = rgi_convert_generic; /* sic! */
    }
    return stdfunc;
}


void
rgi_convert_generic (rgi_surface *s_dst, rgi_surface *s_src)
{
    int i, j;
    uint32 o1, o2;
    uint8 pix8 = 0, pix8b = 0, pix8c = 0;
    uint16 pix16 = 0;
    uint32 pix32 = 0;
    uint8 r = 0, g = 0, b = 0;

    uint8 *src8 = (uint8 *) s_src -> pixels;
    uint16 *src16 = (uint16 *) s_src -> pixels;
    uint32 *src32 = (uint32 *)s_src -> pixels;

    uint8 *dst8 = (uint8 *) s_dst -> pixels;
    uint16 *dst16 = (uint16 *) s_dst -> pixels;
    uint32 *dst32 = (uint32 *) s_dst -> pixels;
    
    o1 = o2 = 0;
    for (j = 0; j < s_src -> h; j++)
    {
        for (i = 0; i < s_src -> w; i++)
        {
            switch (s_src -> format -> BitsPerPixel)
            {
            case 8:
                pix8 = src8[o1++];
                switch (s_dst -> format -> BitsPerPixel)
                {
                case 16:
                    pix16 = s_src -> format -> keys16bits[pix8];
                    break;
                case 24:
                    break;
                case 32:
                    pix32 = s_src -> format -> keys32bits[pix8];
                    break;
                }
                break;

            case 16:
                pix16 = src16[o1++];
                switch (s_dst -> format -> BitsPerPixel)
                {
                case 8:
                    rgi_get_color_component (s_src -> format, pix16, &r, &g, &b);
                    pix8 = (r + g + b) * 341 >> 10;
                    break;
                case 24:
                    break;
                case 32:
                    rgi_get_color_component (s_src -> format, pix16, &r, &g, &b);
                    pix32 = rgi_build_color_key (s_dst -> format, r, g, b);
                    break;
                }
                break;
            case 24:
                pix8 = src8[o1++];
                pix8b = src8[o1++];
                pix8c = src8[o1++];
                break;
            case 32:
                pix32 = src32[o1++];
                break;
            }

            switch (s_dst -> format -> BitsPerPixel)
            {
            case 8:
                dst8[o2++] = pix8;
                break;
            case 16:
                dst16[o2++] = pix16;
                break;
            case 32:
                dst32[o2++] = pix32;
                break;
            }
        }
    }
}
