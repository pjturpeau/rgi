#ifndef RGI_8INDEXED_H
#define RGI_8INDEXED_H

#ifdef __USE_X86_ASSEMBLER__
extern void rgi_convert_x86_8INDEXED_16RGB565(rgi_surface *s_dst,
					      rgi_surface *s_src);
extern void rgi_convert_x86_8INDEXED_32RGB888(rgi_surface *s_dst,
					      rgi_surface *s_src);
#endif /* __USE_X86_ASSEMBLER__ */

extern void rgi_convert_LE_C_8INDEXED_32RGB888(rgi_surface *s_dst,
					       rgi_surface *s_src);
extern void rgi_convert_LE_C_8INDEXED_16RGB565(rgi_surface *s_dst,
					       rgi_surface *s_src);

#endif /* RGI_8INDEXED_H */
