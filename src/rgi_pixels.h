#ifndef RGI_PIXELS_H
#define RGI_PIXELS_H

/*
  Here we define structures and functions use to build a pixel
  structure description for a surface. It defines the number of bits
  per pixel that is used to build a color value in the surface and
  also the number of bytes per pixel (a surface that have 15bpp uses 2
  bytes per pixel). In a hi-color mode, a color value is the
  combination of 3 color components plus an attribute : Red, Green,
  Blue and Alpha. In an indexed mode (up to 8bpp), a color value is an
  index in a palette array.

  Hi-Color Modes :
   
  Rmask, Gmask, Bmask and Amask are used to select (with a logical OR)
  bits of the associated color component.

  Rshift, Gshift, Bshift and Ashift are used to get (set), as a 8bits
  value, with a right (left) shift, the associated color component from
  (to) a color value.

  Rloss, Gloss, Bloss and Aloss represent the number of bits that are
  lost for each color component comparing to a full 32bits (8x8x8x8)
  mode.

  Rcomp, Gcomp, Bcomp and Acomp are the number of bits to code each
  color component.

*/

typedef struct pixel_struct
{
  rgi_palette *palette;
  uint16 *keys16bits;
  uint32 *keys32bits;

  uint8 BitsPerPixel;
  uint8 BytesPerPixel;

  /* masks are only bits that correspond to a specific color component
     in a colorkey value */
  uint32 Rmask;			/* Red mask */
  uint32 Gmask;
  uint32 Bmask;
  uint32 Amask;
  
  uint8 Rshift;
  uint8 Gshift;
  uint8 Bshift;
  uint8 Ashift;
  
  uint8 Rloss;
  uint8 Gloss;
  uint8 Bloss;
  uint8 Aloss;

  uint8 Rcomp;			/* Red component's bits */
  uint8 Gcomp;
  uint8 Bcomp;
  uint8 Acomp; 

} rgi_pixel_format;

extern rgi_pixel_format *rgi_create_pixel_format (int bpp, uint32 Rmask,
						  uint32 Gmask, uint32 Bmask,
						  uint32 Amask);

extern void rgi_free_pixel_format (rgi_pixel_format *pf);

extern rgi_pixel_format *rgi_create_rgb_pixel_format (int bpp);

extern void rgi_write_pixel_format (rgi_pixel_format *pf);

extern int rgi_compare_pixel_format (rgi_pixel_format *pf1,
				     rgi_pixel_format *pf2);

extern uint32 rgi_build_color_key (rgi_pixel_format *dst, 
				   uint8 r, uint8 g, uint8 b);

extern void rgi_get_color_component (rgi_pixel_format *src, uint32 colorkey,
				     uint8 *r, uint8 *g, uint8 *b);

extern void rgi_pixel_format_set_bits_per_pixel (rgi_pixel_format *pf, int bpp);

#endif /* RGI_PIXELS_H */
