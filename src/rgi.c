#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef HAVE_CONFIG_H
#include "rgi_config.h"
#endif /* HAVE_CONFIG_H */

#include "rgi.h"

char rgi_app_name[STATIC_STRING];
char rgi_loop = 0;

static void rgi_parse (int argc, char *argv[]);
void rgi_dummy_usage (int type);

void (*rgi_user_usage) (int type) = rgi_dummy_usage;

void
rgi_init (int argc, char *argv[], char *app_name)
{
    /*
      uint32 test = 0x11223344;
      uint32 *t = &test;
      uint8 *c = (uint8 *) &test;
      uint16 *w = (uint16 *) &test;
    */

    rgi_banner ("RGI");
    rgi_parse (argc, argv);

    rgi_set_app_name (app_name);
  
    rgi_init_convert ();
    rgi_init_video ();

    /* init the sound */
    /* init some weird thingz */

    rgi_loop = 1;
    rgi_sys_kill_signals ();
  
    /*
      printf ("endianness: %X\n", *t);
      printf ("endianness: %X%X%X%X\n", *c, *(c+1), *(c+2), *(c+3));
      printf ("endianness: %X%X\n", *w, *(w+1));
    */
}

void
rgi_set_app_name (char *app_name)
{
    app_name
        ? strncpy (rgi_app_name, app_name, STATIC_STRING)
        : strcpy (rgi_app_name, "RGI_APP");
}

void
rgi_quit ()
{
    /* close all */
    rgi_close_video ();
}

void
rgi_write_infos (rgi_viewport *v)
{
    printf ("\n");
    rgi_write_video_infos (v);
    rgi_sys_write_os_infos ();
    printf (" Frame Per Second       : %g\n\n", rgi_get_fps ());
}

void
rgi_compile_info ()
{
    printf (" Compile defines :\n");
#ifdef NDEBUG
    printf ("\t - NDEBUG assert macro removed\n");
#endif /* NDEBUG */
#ifdef VERY_VERBOSE
    printf ("\t - VERY_VERBOSE very verbose mode\n");
#endif /* VERY_VERBOSE */
#ifdef HAVE_X11_EXTENSIONS_XSHM_H
    printf ("\t - HAVE_X11_EXTENSIONS_XSHM_H shared memory extension\n");
#endif /* HAVE_X11_EXTENSIONS_XSHM_H */
#ifdef HAVE_X11_EXTENSIONS_XF86DGA_H
    printf ("\t - HAVE_X11_EXTENSIONS_XF86DGA_H direct graphic access\n");
#endif /* HAVE_X11_EXTENSIONS_XF86DGA_H */
#ifdef HAVE_X11_EXTENSIONS_XF86VMODE_H
    printf ("\t - HAVE_X11_EXTENSIONS_XF86VMODE_H vidmode extension\n");
#endif /* HAVE_X11_EXTENSIONS_XF86VMODE_H */
#ifdef __USE_X86_ASSEMBLER__
    printf ("\t - __USE_X86_ASSEMBLER__ pentium assembler\n");
#endif /* __USE_X86_ASSEMBLER__ */

    printf ("\n");
    exit (0);
}

void
rgi_dummy_usage (int type)
{
    if (type == 0) {
        printf ("\n");
    } else if (type == 1) {
    }
}

static void
rgi_usage (char *app)
{
    printf ("Usage : \n");
    printf ("   %s [-i] [-h]", app);
    rgi_user_usage (0);
    printf ("\t -i   RGI compile infos\n");
    printf ("\t -h   Help\n");
    rgi_user_usage (1);
    printf ("\n");
    exit (0);
}

static void
rgi_parse (int argc, char *argv[])
{
    int i;

    i = 1;
    while (i < argc) {
        char *str = argv[i];
        if (*str == '-') {
            switch (*(str + 1)) {
            case 'i':
                rgi_compile_info ();
                break;
            case 'h':
                rgi_usage (argv[0]);
                break;
            }
        }
        i++;
    }
}

void
rgi_error (const char *err)
{
    fprintf (stderr, " [FATAL] *** %s\n", err);
    rgi_quit ();
    exit (EXIT_FAILURE);
}
