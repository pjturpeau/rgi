#include <stdlib.h>

#ifdef HAVE_CONFIG_H
#include "rgi_config.h"
#endif /* HAVE_CONFIG_H */

#include "rgi_types.h"
#include "rgi_colors.h"
#include "rgi_pixels.h"
#include "rgi_surface.h"
#include "rgi_16rgb.h"

/*
 * Little Endian 16 bit RGB to 32 bit RGB
 */
void
rgi_convert_LE_C_16RGB565_32RGB888 (rgi_surface *s_dst, rgi_surface *s_src)
{
  uint32 i, o1, o2;
  uint32 r, g, b;
  uint32 pix, cnv;
  uint32 r2, g2, b2;

  uint32 *ptr = (uint32 *) s_src -> pixels;
  uint32 *dest = (uint32 *) s_dst -> pixels;

  o1 = 0;
  o2 = 0;
  for (i = 0; i < s_src->size; i += 2)
    {
      pix = ptr[o1++];
      
      r = g = b = pix & 65535;
      b = b & 0x1F;
      g = (g >> 5) & 0x3F;
      r = (r >> 11) & 0x1F;

      r2 = g2 = b2 = (pix >> 16) & 65535;
      b2 = b2 & 0x1F;
      g2 = (g2 >> 5) & 0x3F;
      r2 = (r2 >> 11) & 0x1F;

      cnv = (r << 3);
      cnv = (cnv << 8) + (g << 2);
      cnv = (cnv << 8) + (b << 3);

      dest[o2++] = cnv;
      
      cnv = (r2 << 3);
      cnv = (cnv << 8) + (g2 << 2);
      cnv = (cnv << 8) + (b2 << 3);
      
      dest[o2++] = cnv;
    } 
}

/*
 * Little Endian 16->8 (gray)
 */
void
rgi_convert_LE_C_16RGB565_8INDEXED (rgi_surface *s_dst, rgi_surface *s_src)
{
  uint32 i, o1, o2;
  uint32 r, g, b;
  uint32 pix, cnv1, cnv2, cnv3, cnv4;
  uint32 r2, g2, b2;

  uint32 *ptr = (uint32 *) s_src -> pixels;
  uint32 *dest = (uint32 *) s_dst -> pixels;

  o1 = 0;
  o2 = 0;
  for (i = 0; i < (s_src->size >> 2); i++)
    {
      pix = ptr[o1++];
      
      r = g = b = pix & 65535;
      b = b & 0x1F;
      g = (g >> 6) & 0x1F;
      r = (r >> 11) & 0x1F;
      
      cnv1 = ((r + g + b) * 341) >> 7;

      r2 = g2 = b2 = (pix >> 16) & 65535;
      b2 = b2 & 0x1F;
      g2 = (g2 >> 6) & 0x1F;
      r2 = (r2 >> 11) & 0x1F;

      cnv2 = ((r2 + g2 + b2) * 341) >> 7;
      
      pix = ptr[o1++];
      
      r = g = b = pix & 65535;
      b = b & 0x1F;
      g = (g >> 6) & 0x1F;
      r = (r >> 11) & 0x1F;
      
      cnv3 = ((r + g + b) * 341) >> 7;

      r2 = g2 = b2 = (pix >> 16) & 65535;
      b2 = b2 & 0x1F;
      g2 = (g2 >> 6) & 0x1F;
      r2 = (r2 >> 11) & 0x1F;

      cnv4 = ((r2 + g2 + b2) * 341) >> 7;
      
      dest[o2++] = (cnv4 << 24) + (cnv3 << 16) + (cnv2 << 8) + cnv1;      
    }
}

