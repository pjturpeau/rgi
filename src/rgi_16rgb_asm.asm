//
//
//
	#ifdef HAVE_CONFIG_H
	#include "TMS_config.h"
	#endif
	#include "TMS_offsets.h"
	
	.file "TMS_SYS_16RGB_S.S"

//=============================================================================
	.data
//=============================================================================

	.align	4
	
//=============================================================================
	.text
//=============================================================================

#ifdef __USE_X86_ASSEMBLER__

ENTRY(TMS_Convert_x86_16RGB565_32RGB888)
	pushl	%ebp
	movl	%esp, %ebp
	pushal

	movl	8(%ebp), %ebx	// ebx = *s_dst
	movl	12(%ebp), %edx	// ebp = *s_src
	movl	SURFACE_PIXELS(%ebx), %edi
	mov	SURFACE_SIZE(%edx), %ebp
	movl	SURFACE_PIXELS(%edx), %esi
	shrl	$1, %ebp
Loop0001:
	movl	(%esi), %eax
	movl	%eax, %ebx
	movl	%eax, %edx

	pushl	%edx
	
	shrl	$5, %ebx
	shrl	$11, %eax
	andl	$0x1F, %edx
	andl	$0x3F, %ebx
	andl	$0x1F, %eax

	shll	$11, %eax
	shll	$2, %ebx
	addl	%ebx, %eax
	shll	$3, %edx
	shll	$8, %eax
	addl	%edx, %eax
	
	movl	%eax, (%edi)

	popl	%eax
	shrl	$16, %eax
	movl	%eax, %ebx
	movl	%eax, %edx

	shrl	$5, %ebx
	shrl	$11, %eax
	andl	$0x1F, %edx
	andl	$0x3F, %ebx
	andl	$0x1F, %eax

	shll	$11, %eax
	shll	$2, %ebx
	addl	%ebx, %eax
	shll	$3, %edx
	shll	$8, %eax
	addl	%edx, %eax
	
	movl	%eax, 4(%edi)
	
	addl	$4, %esi
	addl	$8, %edi
	
	dec	%ebp
	jnz	Loop0001
	
	popal
	leave
	ret




	
ENTRY(TMS_Convert_x86_16RGB565_8INDEXED)
	pushl	%ebp
	movl	%esp, %ebp
	pushal

	movl	8(%ebp), %ebx	// ebx = *s_dst
	movl	12(%ebp), %edx	// edx = *s_src
	movl	SURFACE_PIXELS(%ebx), %edi
	movl	SURFACE_SIZE(%edx), %ebp
	movl	SURFACE_PIXELS(%edx), %esi
	shrl	$2, %ebp
Loop0002:
	movl	(%esi), %eax
	movl	%eax, %ebx
	movl	%eax, %edx

	pushl	%edx
	
	shrl	$6, %ebx
	shrl	$11, %eax
	andl	$0x1F, %edx	// b
	andl	$0x1F, %ebx	// g
	andl	$0x1F, %eax	// r

	addl	%ebx, %eax
	addl	%edx, %eax
	imul	$341, %eax
	shrl	$7, %eax
	
	popl	%edx

	pushl	%eax
	
	shrl	$16, %edx
	movl	%edx, %ebx
	movl	%edx, %eax

	shrl	$6, %ebx
	shrl	$11, %eax
	andl	$0x1F, %edx	// b
	andl	$0x1F, %ebx	// g
	andl	$0x1F, %eax	// r

	addl	%ebx, %eax
	addl	%edx, %eax
	imul	$341, %eax
	shrl	$7, %eax

	pushl	%eax

//--- rE-GaSP!	
	movl	4(%esi), %eax
	movl	%eax, %ebx
	movl	%eax, %edx

	pushl	%edx
	
	shrl	$6, %ebx
	shrl	$11, %eax
	andl	$0x1F, %edx	// b
	andl	$0x1F, %ebx	// g
	andl	$0x1F, %eax	// r

	addl	%ebx, %eax
	addl	%edx, %eax
	imul	$341, %eax
	shrl	$7, %eax
	
	popl	%edx

	pushl 	%eax
	
	shrl	$16, %edx
	movl	%edx, %ebx
	movl	%edx, %eax

	shrl	$6, %ebx
	shrl	$11, %eax
	andl	$0x1F, %edx	// b
	andl	$0x1F, %ebx	// g
	andl	$0x1F, %eax	// r

	addl	%ebx, %eax
	addl	%edx, %eax
	imul	$341, %eax
	shrl	$7, %eax

	shll	$16, %eax
	popl	%ebx
	popl	%ecx
	popl	%edx

	movb	%bl, %ah
	movb	%cl, %al
	shll	$8, %eax
	movb	%dl, %al

	movl	%eax, (%edi)
	
	addl	$8, %esi
	addl	$4, %edi
	
	dec	%ebp
	jnz	Loop0002
	
	popal
	leave
	ret

	
#endif /* __USE_X86_ASSEMBLER__ */


/* 0ld $k00l3rS uS3 aSs3m8l3r! */

