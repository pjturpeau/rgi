#include <stdlib.h>
#include <assert.h>

#ifdef HAVE_CONFIG_H
#include "rgi_config.h"
#endif /* HAVE_CONFIG_H */

#include "rgi_types.h"
#include "rgi_colors.h"

void
rgi_free_color (rgi_color *c)
{
    free(c);
}

rgi_color * 
rgi_new_color ()
{
    rgi_color *c;

    c = (rgi_color *) malloc (sizeof (rgi_color));
    assert (c);

    return c;
}

rgi_color *
rgi_create_color (uint8 r, uint8 g, uint8 b, uint8 a)
{
    rgi_color *c;

    c = rgi_new_color ();
    rgi_set_color (c, r, g, b, a);
    return c;
}


void
rgi_copy_color (rgi_color *d, rgi_color *s)
{
    memcpy (d, s, sizeof (rgi_color));
}

void
rgi_set_color (rgi_color *c, uint8 r, uint8 g, uint8 b, uint8 a)
{
    c -> r = r;
    c -> g = g;
    c -> b = b;
    c -> a = a;
}


void
rgi_free_palette (rgi_palette *p)
{
    int i;

    for (i = 0; i < p -> ncolors; i++)
        free (p -> colors[i]);
    free (p -> colors);
    free (p);
}


rgi_palette *
rgi_new_palette ()
{
    rgi_palette *p;

    p = (rgi_palette *) malloc (sizeof (rgi_palette));
    assert (p);

    return p;
}


rgi_palette *
rgi_create_palette (int ncolors)
{
    rgi_palette *p;
    int i;

    p = rgi_new_palette ();
    p -> ncolors = ncolors;

    if (p -> ncolors)
    {
        p -> colors = (rgi_color **) malloc (sizeof (rgi_color *)
                                             * p -> ncolors);
        assert (p);
        for (i = 0; i < p -> ncolors; i++)
            p -> colors[i] = rgi_new_color();
    }

    return p;
}


void
rgi_copy_palette(rgi_palette *d, rgi_palette *s)
{
    int i;

    for (i = 0; (i < s -> ncolors) && (i < d -> ncolors); i++)
        rgi_copy_color (d -> colors[i], s -> colors[i]);
}
