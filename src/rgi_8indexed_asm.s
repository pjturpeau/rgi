	bits 32

	%include "rgi_nasm_offsets.h"


	section .text

%ifdef __USE_X86_ASSEMBLER__

	global	_rgi_convert_x86_8INDEXED_16RGB565
_rgi_convert_x86_8INDEXED_16RGB565:
	push	ebp
	mov	ebp, esp
	pushad

	mov	ebx, [ebp + 8]	; ebx = *s_dst
	mov	edx, [ebp + 12]	; edx = *s_src
	mov	edi, [ebx + SURFACE_PIXELS]
	mov	esi, [edx + SURFACE_PIXELS]
	mov	ebp, [edx + SURFACE_SIZE]	; ebp = s_src->size 
	mov	ebx, [edx + SURFACE_FORMAT]	; ebx = s_src->format
	shr	ebp, 2				; we read 4 pixels at a time 
	mov	ebx, [ebx + PIXELFORMAT_KEYS16BITS]

.Loop0001
	xor	edx, edx
	mov	eax, [esi]

	mov	dl, ah

	mov	cx, [ebx + edx * 2]
	mov	dl, al
	shl	ecx, 16

	mov	cx, [ebx + edx * 2]
	shr	eax, 16
	mov	[edi], ecx

	mov	dl, ah
	mov	cx, [ebx + edx * 2]
	mov	dl, al
	shl	ecx, 16
	mov	cx, [ebx + edx * 2]

	add	esi, 4

	mov	[edi + 4], ecx

	add	edi, 8
		
	dec	ebp
	jnz	.Loop0001
	
	popad
	leave
	ret

	

	
	global	_rgi_convert_x86_8INDEXED_32RGB888
_rgi_convert_x86_8INDEXED_32RGB888:
	push	ebp
	mov	ebp, esp
	pushad

	mov	ebx, [ebp + 8]	; ebx = *s_dst
	mov	edx, [ebp + 12]	; edx = *s_src
	mov	edi, [ebx + SURFACE_PIXELS]
	mov	esi, [edx + SURFACE_PIXELS]
	mov	ebp, [edx + SURFACE_SIZE]	; ebp = s_src->size 
	mov	ebx, [edx + SURFACE_FORMAT]	; ebx = s_src->format
	shr	ebp, 2				; we read 4 pixels at a time 
	mov	ebx, [ebx + PIXELFORMAT_KEYS32BITS]

.Loop0001
	xor	edx, edx
	mov	eax, [esi]

	mov	dl, al

	mov	ecx, [ebx + edx * 4]
	mov	dl, ah
	mov	[edi], ecx

	mov	ecx, [ebx + edx * 4]
	shr	eax, 16
	mov	[edi + 4], ecx

	mov	dl, al
	mov	ecx, [ebx + edx * 4]
	mov	[edi + 8], ecx

	mov	dl, ah
	mov	ecx, [ebx + edx * 4]
	mov	[edi + 12], ecx

	add	esi, 4
	add	edi, 16
		
	dec	ebp
	jnz	.Loop0001
	
	popad
	leave
	ret
	
%endif	; __USE_X86_ASSEMBLER__
