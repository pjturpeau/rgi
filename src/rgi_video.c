#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#ifdef HAVE_CONFIG_H
#include "rgi_config.h"
#endif /* HAVE_CONFIG_H */

#include "rgi_types.h"
#include "rgi_colors.h"
#include "rgi_pixels.h"
#include "rgi_surface.h"
#include "rgi_convert.h"
#include "rgi_video.h"

void
rgi_init_video ()
{
    rgi_sys_init_video ();
}

void
rgi_close_video ()
{
    rgi_sys_close_video ();
}

rgi_viewport *
rgi_set_videomode (int width, int height, int bpp, uint32 flags)
{
    rgi_palette *p;
    rgi_viewport *v;
    int i;

    v = rgi_sys_set_videomode (width, height, bpp, flags);
    rgi_sys_set_appname (v);

    v -> convert = rgi_dummy;

    if (v -> buffer != v -> screen)
    {
        v -> convert = rgi_get_convert_function (v -> screen, v -> buffer);

        if (v -> screen -> format -> BitsPerPixel == 8)
        {
            p = rgi_create_palette (256);

            for (i = 0; i < 256; i++)
            {
                p -> colors[i] -> r = i;
                p -> colors[i] -> g = i;
                p -> colors[i] -> b = i;
                p -> colors[i] -> a = 0xFF;
            }
	  
            rgi_set_palette (p, v);
            rgi_free_palette (p);	  
        }       
    }
    rgi_write_video_infos (v);
    return v;
}

void
rgi_close_videomode (rgi_viewport *v)
{
    rgi_sys_free_viewport (v);
}

void
rgi_update_video (rgi_viewport *v)
{
    v -> convert (v -> screen, v -> buffer);			  
    rgi_sys_update_video (v);
}

void
rgi_free_viewport (rgi_viewport *v)
{
    rgi_sys_free_viewport (v);
}

void
rgi_write_video_infos (rgi_viewport *v)
{
    printf (" Video screen format    : %dx%dx%d - ",
            v -> screen -> w, v -> screen -> h,
            v -> screen -> format -> BitsPerPixel);

    (v -> screen -> format -> Acomp == 0)
        ? ((v -> screen -> format -> Acomp == v -> screen -> format -> Gcomp
            && v -> screen -> format -> Rcomp == v -> screen -> format -> Bcomp
            && v -> screen -> format -> Acomp == v -> screen -> format -> Rcomp)
           ? printf ("Indexed\n") : printf ("RGB %d%d%d\n",
                                            v -> screen -> format -> Rcomp,
                                            v -> screen -> format -> Gcomp,
                                            v -> screen -> format -> Bcomp) )
        :  printf ("ARGB %d%d%d%d\n",
                   v -> screen -> format -> Acomp, v -> screen -> format -> Rcomp,
                   v -> screen -> format -> Gcomp, v -> screen -> format -> Bcomp);

    printf (" IO buffer format       : %dx%dx%d - ", v -> buffer -> w,
            v -> buffer -> h, v -> buffer -> format -> BitsPerPixel);

    (v -> buffer -> format -> Acomp == 0) ?
        ((v -> buffer -> format -> Acomp == v -> buffer -> format -> Gcomp &&
          v -> buffer -> format -> Rcomp == v -> buffer -> format -> Bcomp &&
          v -> buffer -> format -> Acomp == v -> buffer -> format -> Rcomp) ?    
         printf ("Indexed\n") : printf ("RGB %d%d%d\n",
                                        v -> buffer -> format -> Rcomp,
                                        v -> buffer -> format -> Gcomp,
                                        v -> buffer -> format -> Bcomp) )
        :  printf ("ARGB %d%d%d%d\n",
                   v -> buffer -> format -> Acomp, v -> buffer -> format -> Rcomp,
                   v -> buffer -> format -> Gcomp, v -> buffer -> format -> Bcomp);

    rgi_sys_write_video_infos (v);
  
    printf (" Assembler routines     : ");
#ifndef __USE_X86_ASSEMBLER__
    printf ("No\n");
#else  /* __USE_X86_ASSEMBLER__ */
    if (rgi_compare_pixel_format (v -> screen -> format,
                                  v -> buffer -> format) != 0)
        printf ("Yes\n");
    else
        printf ("Unused\n");
#endif /* __USE_X86_ASSEMBLER__ */
}


void
rgi_set_palette (rgi_palette *p, rgi_viewport *v)
{
    int i;
    rgi_pixel_format *bf = v -> buffer -> format;
    rgi_pixel_format *sf = v -> screen -> format;
  
    if (v && p && (p -> ncolors <= 256))
    {

        if (sf -> BitsPerPixel == 8)
        {
            rgi_copy_palette (sf -> palette, p);
            rgi_sys_set_palette (p, v);
        }
        else if (bf -> BitsPerPixel == 8)
        {
            rgi_copy_palette (bf -> palette, p);

            if (sf -> BitsPerPixel > 8)
            {

                if (sf -> BitsPerPixel == 16)
                {
                    bf -> keys16bits =
                        (uint16 *) malloc (p -> ncolors * sizeof (uint16));
                    assert (bf -> keys16bits);
                }

                else if (sf -> BitsPerPixel == 24 || sf -> BitsPerPixel == 32)
                {
                    bf -> keys32bits =
                        (uint32 *) malloc (p -> ncolors * sizeof (uint32));
                    assert (bf -> keys32bits);
                }

                for (i = 0; i < p -> ncolors; i++)
                {
                    switch (sf -> BitsPerPixel)
                    {
                    case 16:
                        bf -> keys16bits[i] =
                            (uint16) rgi_build_color_key(sf,
                                                         p -> colors[i] -> r,
                                                         p -> colors[i] -> g,
                                                         p -> colors[i] -> b);
                        break;
                    case 24:
                    case 32:
                        bf -> keys32bits[i] = 
                            rgi_build_color_key (sf,
                                                 p -> colors[i] -> r,
                                                 p -> colors[i] -> g,
                                                 p -> colors[i] -> b);
                        break;
                    }
                }
            }
            // we don't need to set XWindow colormap since screen is not in PseudoColor...
            //rgi_sys_set_palette(p, v);
        }
    }
}

void
rgi_set_colors (rgi_viewport *v, rgi_color *c, int start, int end)
{
    int i;

    if (v -> buffer -> format -> palette &&
        end <= v -> buffer -> format -> palette -> ncolors)
    {
        for (i = start; i < end; i++)
            rgi_set_color (v -> buffer -> format -> palette -> colors[i],
                           c[i].r, c[i].g, c[i].b, c[i].a);

        rgi_set_palette (v -> buffer -> format -> palette, v);
    }
    else if (v -> screen -> format -> palette &&
             end <= v -> screen -> format -> palette -> ncolors)
    {
        for (i = start; i < end; i++)
            rgi_set_color (v -> screen -> format -> palette -> colors[i],
                           c[i].r, c[i].g, c[i].b, c[i].a);

        rgi_set_palette (v -> screen -> format -> palette, v);
    }
}

