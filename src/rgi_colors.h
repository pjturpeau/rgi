#ifndef RGI_COLORS_H
#define RGI_COLORS_H

typedef struct color_struct {

  uint8 r;			/* 0 - 255 */
  uint8 g;			/* 0 - 255 */
  uint8 b;			/* 0 - 255 */
  uint8 a;			/* unused for the moment */

} rgi_color;

typedef struct palette_struct {

  rgi_color **colors;
  int ncolors;

} rgi_palette;

extern rgi_color *rgi_create_color (uint8 r, uint8 g, uint8 b, uint8 a);
extern void rgi_free_color (rgi_color *c);
extern void rgi_set_color (rgi_color *c, uint8 r, uint8 g, uint8 b, uint8 a);
extern void rgi_copy_color (rgi_color *d, rgi_color *s);
extern rgi_palette *rgi_create_palette (int ncolors);
extern void rgi_free_palette (rgi_palette *p);
extern void rgi_copy_palette (rgi_palette *d, rgi_palette *s);

#endif /* RGI_COLORS_H */
