#ifndef RGI_CONVERT_H
#define RGI_CONVERT_H

typedef struct rgi_convinfo_struct
{
  uint8 srcbpp; 
  uint32 srcRmask;
  uint32 srcGmask;
  uint32 srcBmask;

  uint8 dstbpp; 
  uint32 dstRmask;
  uint32 dstGmask;
  uint32 dstBmask;

  void (*convert_func) (rgi_surface *dst, rgi_surface *src);
} rgi_convert_info;

typedef void (*rgi_convert_fun) (rgi_surface *s_dst, rgi_surface *s_src);

extern void rgi_init_convert ();
extern void rgi_dummy (rgi_surface *s_dst, rgi_surface *s_src);
extern void rgi_convert_generic (rgi_surface *s_dst, rgi_surface *s_src);
rgi_convert_fun rgi_get_convert_function (rgi_surface *s_dst, rgi_surface *s_src);



#endif /* RGI_CONVERT_H */
