#ifndef RGI_SYS_TOOLS_H
#define RGI_SYS_TOOLS_H

extern void rgi_sys_kill_signals ();
extern void rgi_sys_clear_kbd_buffer (rgi_viewport *v);
extern void rgi_sys_write_os_infos ();
extern inline void rgi_sys_check_events (rgi_viewport *v);

#endif /* RGI_SYS_TOOLS_H */
