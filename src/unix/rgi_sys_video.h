#ifndef RGI_SYS_VIDEO_C
#define RGI_SYS_VIDEO_C

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xos.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <X11/Xresource.h>

#ifdef HAVE_CONFIG_H
#include "rgi_config.h"
#endif /* HAVE_CONFIG_H */

#ifdef HAVE_X11_EXTENSIONS_XSHM_H
#include <X11/extensions/XShm.h>

#ifdef HAVE_SYS_IPC_H
#include <sys/ipc.h>
#endif /* HAVE_SYS_IPC_H */

#ifdef HAVE_SYS_SHM_H
#include <sys/shm.h>
#endif /* HAVE_SYS_SHM_H */

#endif /* HAVE_X11_EXTENSIONS_XSHM_H */

#ifdef HAVE_X11_EXTENSIONS_XF86DGA_H
#include <X11/extensions/xf86dga.h>
#endif /* HAVE_X11_EXTENSIONS_XF86DGA_H */

#ifdef HAVE_X11_EXTENSIONS_XF86VMODE_H
#include <X11/extensions/xf86vmode.h>
#endif /* HAVE_X11_EXTENSIONS_XF86VMODE_H */

#define RGI_NO_VIDEO		0
#define RGI_X11_VIDEO		(1<<0)
#define RGI_X11SHM_VIDEO	(1<<1)
#define RGI_X11SHMPIXMAP_VIDEO	(1<<2)
#define RGI_X11DGA_VIDEO	(1<<3)
#define RGI_SVGALIB_VIDEO	(1<<4)
#define RGI_GGI_VIDEO		(1<<5)


typedef struct viewport_struct {

  /* Here we got System Independant fields */
  rgi_surface *buffer;		/* public */
  rgi_surface *screen;		/* private */

  rgi_convert_fun convert;

  /* Here we got system dependant fields */
  Window window;
  XImage *ximage;
  Cursor cursor;
  GC gc;
  Colormap colormap;

  int VideoAccessType;
  uint32 *palette;

#ifdef HAVE_X11_EXTENSIONS_XSHM_H
  Pixmap pixmap;
  XShmSegmentInfo *shmseginfo;
#endif /* HAVE_X11_EXTENSIONS_XSHM_H */

#ifdef HAVE_X11_EXTENSIONS_XF86DGA_H
  char *DGAaddr;
  int DGAwidth;
  int DGAwidthsize;		/* = DGAwidth * realpixelsize */
  int DGAbank;
  int DGAbanks;
  int DGAram;
#endif /* HAVE_X11_EXTENSIONS_XF86DGA_H */

} rgi_viewport;

extern Display *rgi_display;

extern void rgi_sys_init_video ();
extern void rgi_sys_close_video ();

extern rgi_viewport *rgi_sys_set_videomode (int width, int height,
					    int depth, uint32 flags);

extern void rgi_sys_set_appname (rgi_viewport *v);
extern void rgi_sys_update_video(rgi_viewport *v);
extern void rgi_sys_free_viewport(rgi_viewport *v);
extern void rgi_sys_write_video_infos(rgi_viewport *v);
extern void rgi_sys_set_palette(rgi_palette *p, rgi_viewport *v);

#endif /* RGI_SYS_VIDEO_H */
