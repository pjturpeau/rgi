#include <stdlib.h>
#include <stdio.h>
#include <signal.h>

#ifdef HAVE_CONFIG_H
#include "rgi_config.h"
#endif /* HAVE_CONFIG_H */

#include "rgi.h"
#include "unix/rgi_sys_tools.h"

static void
OnBreak (int arg)
{
    rgi_loop = 0;
    printf ("[rgi] *** Break signal catched!\n");
}

void
rgi_sys_kill_signals ()
{
    signal (SIGHUP, OnBreak);
    signal (SIGINT, OnBreak);
    signal (SIGQUIT, OnBreak);
    signal (SIGTERM, OnBreak);
}

void
rgi_sys_clear_kbd_buffer (rgi_viewport *v)
{
    XEvent ev;
    long KEY;

    while (XCheckWindowEvent (rgi_display, v -> window, KeyPressMask, &ev)) {
        KEY = XLookupKeysym ((XKeyEvent *) &ev, 0);  
    }
}

void
rgi_sys_write_os_infos ()
{
    FILE *f;
    char OS[64];
  
    f = popen ("uname -s -r", "r");
    if (!f) strcpy (OS, "Unknown\n");
    else
    {
        fgets (OS, 64, f);
        pclose (f);
    }
    printf (" Operating System       : %s", OS);
}

inline void
rgi_sys_check_events (rgi_viewport *v)
{
    XEvent ev;
    long KEY;
    char *atom_name; 
 
    while (XPending (rgi_display)) {
        XNextEvent (rgi_display, &ev);
        switch (ev.type) {
        case ClientMessage:
            atom_name = XGetAtomName (rgi_display, ev.xclient.message_type);
            if (!strcmp (atom_name, "WM_PROTOCOLS") && ev.xclient.format == 32) {
                atom_name = XGetAtomName (rgi_display, ev.xclient.data.l[0]);
                if (!strcmp(atom_name, "WM_DELETE_WINDOW"))
                    rgi_loop = False;
            }
            break;
        case DestroyNotify:
            rgi_loop = False;
            break;
        case KeyPress:
            KEY = XLookupKeysym ((XKeyEvent *) &ev, 0);
            switch (KEY) {
            case XK_Escape:
                rgi_loop = False;
                break;
            }
            rgi_sys_clear_kbd_buffer (v);
            break;
        case ButtonPress:
            rgi_loop = False;
            break;
        }
    }
}
