#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#ifdef HAVE_CONFIG_H
#include "rgi_config.h"
#endif /* HAVE_CONFIG_H */

#include "rgi.h"
#include "rgi_types.h"
#include "rgi_colors.h"
#include "rgi_pixels.h"
#include "rgi_surface.h"
#include "rgi_convert.h"
#include "rgi_video.h"
#include "unix/rgi_sys_video.h"

/* common things for a given display */
Display *rgi_display = NULL;
Screen *rgi_screen;
int rgi_screennum;
Visual *rgi_visual;
int rgi_depth;
rgi_pixel_format rgi_format;
Window rgi_rootwindow;

int ShmMajor, ShmMinor;
int DGAMajor, DGAMinor;
int DGAEventBase, DGAErrorBase;
Bool XShmPixmaps;

#ifdef HAVE_X11_EXTENSIONS_XF86VMODE_H
XF86VidModeModeInfo **vid_modes;
int nvidmodes;

static XF86VidModeModeLine saved_mode;
static int saved_dotclock;

static int rgi_enumerate_videomodes ();
static void rgi_save_mode ();
static void rgi_restore_mode ();
static void rgi_switch_to_mode (int num);
#endif /* HAVE_X11_EXTENSIONS_XF86VMODE_H */

void
rgi_sys_init_video ()
{
    XPixmapFormatValues *xpix_fmt;
    int i, num_formats;

    rgi_display = XOpenDisplay (NULL);
    if (!rgi_display)
        rgi_error ("Couldn't open display !\n");

    rgi_screen = DefaultScreenOfDisplay (rgi_display);
    rgi_screennum = DefaultScreen (rgi_display);
    rgi_rootwindow = DefaultRootWindow (rgi_display);

    rgi_visual = DefaultVisualOfScreen (rgi_screen);
    rgi_depth = DefaultDepth (rgi_display, rgi_screennum);

    /* Determine the screen depth */
    xpix_fmt = XListPixmapFormats (rgi_display, &num_formats);
    if (!xpix_fmt)
        rgi_error ("Couldn't determine screen formats !\n");

    for (i = 0; (i < num_formats) && (xpix_fmt[i].depth != rgi_depth); i++)
        ;
    if (i < num_formats) {
#ifdef VERY_VERBOSE
        fprintf (stderr, "\n Best pixmap format found on server :\n");
        fprintf (stderr, "   depth          : %d\n", xpix_fmt[i].depth);
        fprintf (stderr, "   bits_per_pixel : %d\n", xpix_fmt[i].bits_per_pixel);
        fprintf (stderr, "   scanline_pad   : %d\n\n", xpix_fmt[i].scanline_pad);
#endif /* VERY_VERBOSE */
        //rgi_format.BitsPerPixel = xpix_fmt[i].bits_per_pixel;
        rgi_pixel_format_set_bits_per_pixel (&rgi_format, xpix_fmt[i].bits_per_pixel);
    }
    XFree ((void *) xpix_fmt);
  
    if (rgi_format.BitsPerPixel > 8) {
        rgi_format.Rmask = rgi_visual -> red_mask;
        rgi_format.Gmask = rgi_visual -> green_mask;
        rgi_format.Bmask = rgi_visual -> blue_mask;
        rgi_format.Amask = 0;
    }

#ifdef HAVE_X11_EXTENSIONS_XF86VMODE_H
    rgi_enumerate_videomodes ();
    rgi_save_mode ();
#endif /* HAVE_X11_EXTENSIONS_XF86VMODE_H */


    /* See whether or not we need to swap pixels */
    /* should have been verified with #ifdef on the system type */
    /*
      swap_pixels = 0;
      if (RGI_BYTEORDER == RGI_LITTLE_ENDIAN )
      {
      if (XImageByteOrder(rgi_display) == MSBFirst)
      swap_pixels = 1;
      }
      else if (XImageByteOrder(rgi_display) == LSBFirst)
      swap_pixels = 1;
    */ 
}


void
rgi_sys_close_video ()
{
#ifdef HAVE_X11_EXTENSIONS_XF86VMODE_H
    rgi_restore_mode ();
#endif /* HAVE_X11_EXTENSIONS_XF86VMODE_H */
    if (rgi_display) {
        XCloseDisplay (rgi_display);
        rgi_display = NULL;
    }
}

void
rgi_sys_init_viewport (rgi_viewport *v)
{
    memset (v, 0, sizeof (rgi_viewport));
    v -> buffer = v -> screen = NULL;
    v -> ximage = NULL;
    v -> convert = NULL;
    v -> VideoAccessType = RGI_NO_VIDEO;
    v -> colormap = None;
}


rgi_viewport *
rgi_sys_new_viewport ()
{
    rgi_viewport *v;
    v = (rgi_viewport *) malloc (sizeof (rgi_viewport));
    assert (v);
    rgi_sys_init_viewport (v);
    return v;
}

void
rgi_sys_free_viewport (rgi_viewport *v)
{
    if (v)
    {
        if (v -> colormap != None)
            XFreeColormap (rgi_display, v -> colormap);
        if (v -> palette != NULL)
            free (v -> palette);
        switch (v -> VideoAccessType) {
        case RGI_X11_VIDEO:
            if (v -> buffer
                && (v -> buffer -> format -> BitsPerPixel
                    != v -> screen -> format -> BitsPerPixel))
                rgi_free_surface (v -> buffer);
	  
            if (v -> ximage)
                XDestroyImage (v -> ximage);

            XFreeGC (rgi_display, v -> gc);
            XDestroyWindow (rgi_display, v -> window);
            break;

#ifdef HAVE_X11_EXTENSIONS_XSHM_H
        case RGI_X11SHM_VIDEO:
            XShmDetach (rgi_display, v -> shmseginfo);
            if (v -> ximage)
                XDestroyImage (v -> ximage);
            if (v -> shmseginfo)
            {
                if (v -> shmseginfo -> shmaddr)
                    shmdt (v -> shmseginfo -> shmaddr);
                if (v -> shmseginfo -> shmid >= 0)
                    shmctl (v -> shmseginfo -> shmid, IPC_RMID, NULL);
                free (v -> shmseginfo);
            }
            if (v -> buffer
                && (v -> buffer -> format -> BitsPerPixel
                    != v -> screen -> format -> BitsPerPixel))
                rgi_free_surface (v -> buffer);
	  
            XFreeGC (rgi_display, v -> gc);
            XDestroyWindow (rgi_display, v -> window);
            break;
	  
        case RGI_X11SHMPIXMAP_VIDEO:
            XShmDetach (rgi_display, v -> shmseginfo);
            XFreePixmap (rgi_display, v -> pixmap);
            if (v -> shmseginfo)
            {
                if (v -> shmseginfo -> shmaddr)
                    shmdt (v -> shmseginfo -> shmaddr);
                if (v -> shmseginfo -> shmid >= 0)
                    shmctl (v -> shmseginfo -> shmid, IPC_RMID, NULL);
                free (v -> shmseginfo);
            }
            if (v -> buffer -> format -> BitsPerPixel != 
                v -> screen -> format -> BitsPerPixel && v -> buffer)
                rgi_free_surface (v -> buffer);
	  
            XFreeGC (rgi_display, v -> gc);
            XDestroyWindow (rgi_display, v -> window);
            break;
#endif /* HAVE_X11_EXTENSIONS_XSHM_H */	  
#ifdef HAVE_X11_EXTENSIONS_XF86DGA_H
        case RGI_X11DGA_VIDEO:
            XF86DGASetVidPage (rgi_display, rgi_screennum, 0);
            XF86DGADirectVideo (rgi_display, rgi_screennum, 0);

            XUngrabPointer (rgi_display, CurrentTime);
            XUngrabKeyboard (rgi_display, CurrentTime);
            break;
#endif /* HAVE_X11_EXTENSIONS_XF86DGA_H */
        }
        free (v);
    } 
}

void
rgi_move_cursor_to (int x, int y)
{
    XWarpPointer (rgi_display, None, rgi_rootwindow, 0, 0, 0, 0, x, y);
}

#ifdef HAVE_X11_EXTENSIONS_XF86VMODE_H
static int
rgi_compare_vidmode (const void *va, const void *vb)
{
    int i;

    XF86VidModeModeInfo *a = *(XF86VidModeModeInfo**) va;
    XF86VidModeModeInfo *b = *(XF86VidModeModeInfo**) vb;
    i = b -> hdisplay - a -> hdisplay;
    if (i == 0)
        return b -> vdisplay - a -> vdisplay;
    else 
        return i;
}

static int
rgi_enumerate_videomodes ()
{
    int i;
    XF86VidModeModeInfo *m;
    int EventBase, ErrorBase;
    int VidModeMajor, VidModeMinor;

    if (!XF86VidModeQueryExtension(rgi_display, &EventBase, &ErrorBase))
        return fprintf (stderr, " [WARNING] *** Can't query XF86VidMode extension (can't switch video mode)\n");

    if (!XF86VidModeQueryVersion(rgi_display, &VidModeMajor, &VidModeMinor))
        return fprintf (stderr, " [WARNING] *** Can't get XF86VidMode version (can't switch video mode)\n");

    if (!XF86VidModeGetAllModeLines(rgi_display, rgi_screennum, &nvidmodes, &vid_modes))
        return fprintf (stderr, " [WARNING] *** Can't get XF86VidMode modelines (can't switch video mode)\n");

    qsort (vid_modes, nvidmodes, sizeof (XF86VidModeModeInfo *), rgi_compare_vidmode);

#ifdef VERY_VERBOSE
    printf (" Available mode list :\n");
    for (i = 0; i < nvidmodes; i++) {
        m = vid_modes[i];
        printf ("\t Mode %d: %dx%d\n", i, m -> hdisplay, m -> vdisplay);
    }
#endif /* VERY_VERBOSE */
    return 0;
}

static void
rgi_save_mode ()
{
    XF86VidModeGetModeLine (rgi_display, rgi_screennum, &saved_dotclock, &saved_mode);
#ifdef VERY_VERBOSE
    printf (" Saved mode :\n");
    printf ("\t %dx%d, %d dotclock\n", saved_mode.hdisplay, saved_mode.vdisplay,
            saved_dotclock);
#endif /* VERY_VERBOSE */
}

static void
rgi_restore_mode ()
{
    int i;
  
    for (i = 0; i < nvidmodes; i++) {
        if (saved_mode.hdisplay == vid_modes[i] -> hdisplay
            && saved_mode.vdisplay == vid_modes[i] -> vdisplay) {
#ifdef VERY_VERBOSE
            printf (" Switching back to default video mode...\n");
#endif /* VERY_VERBOSE */
            rgi_switch_to_mode (i);
            return;
        }      
    }
}

static void
rgi_switch_to_mode (int num)
{
#ifdef VERY_VERBOSE
    XF86VidModeModeInfo *mi;

    mi = vid_modes[num];
    printf (" Switching to mode [%d] %dx%d\n",
            num, mi -> hdisplay, mi -> vdisplay);
#endif /* VERY_VERBOSE */
    
    if (num >= nvidmodes || num < 0)
        return;

    XF86VidModeSwitchToMode (rgi_display, rgi_screennum, vid_modes[num]);
}

static void
rgi_set_viewport_position (int x, int y)
{
    XF86VidModeSetViewPort (rgi_display, rgi_screennum, x, y);
}

int
rgi_select_best_mode (int w, int h)
{
    int i, best;
    XF86VidModeModeInfo *mi;
  
    best = 0;
    for (i = 1; i < nvidmodes; i++) {
        mi = vid_modes[i];
        if (mi -> hdisplay < w || mi -> vdisplay < h)
            return best;
        best = i;
    }
    return best;
}
#endif /* HAVE_X11_EXTENSIONS_XF86VMODE_H */

/*
 * Detect the best video access type available on the system depending
 * on the compilation options : DGA, SHM Pixmap, SHM Std, X11 Std (in
 * order of preference). 
 */
static int
rgi_detect_access_type (rgi_viewport *v)
{
    int access_type;

    /* standard X11 is the default */
    access_type = RGI_X11_VIDEO;

    /* can we get DGA ? */
#ifdef HAVE_X11_EXTENSIONS_XF86DGA_H
    if (!geteuid ()) {		/* root privileges ? */
        if (XF86DGAQueryVersion (rgi_display, &DGAMajor, &DGAMinor))
            if (XF86DGAQueryExtension (rgi_display, &DGAEventBase, &DGAErrorBase))
                if (DGAMajor >= 1) {
                    int dgaflags;

                    XF86DGAQueryDirectVideo (rgi_display, rgi_screennum, &dgaflags);

                    if (dgaflags & XF86DGADirectPresent) {
	    
                        XF86DGAGetVideo (rgi_display, rgi_screennum,
                                         &v -> DGAaddr, &v -> DGAwidth,
                                         &v -> DGAbank, &v -> DGAram);

                        v -> DGAbanks = (v -> DGAram * 1024) / v -> DGAbank;
                        v -> DGAwidthsize = v -> DGAwidth * rgi_format.BytesPerPixel;

                        if ((v -> screen -> w <= v -> DGAwidth) &&
                            (v -> screen -> h <= (v -> DGAbank / v -> DGAwidthsize)))
                            /* we can leave since there's nothing better */
                            return RGI_X11DGA_VIDEO;
                    }
                }
        /* Warn the user about the risk of set-uid apps */
        if (geteuid () != getuid ()) {
            fprintf (stderr, " [WARNING] *** This set-uid program" \
                     "may be a security risk !\n");
        }
    }
#endif /* HAVE_X11_EXTENSIONS_XF86DGA_H */

    /* In case the application is set-uid, we don't need that risk */
    if ((access_type != RGI_X11DGA_VIDEO) && geteuid () == 0)
        setuid (getuid ());

    /* We can't get DGA ! What about shared memory ? */
#ifdef HAVE_X11_EXTENSIONS_XSHM_H
    if (access_type == RGI_X11_VIDEO) {
        /* If we are not on a local display, we can't use shared memory */
        if (strncmp (XDisplayName (NULL), ":", 1) == 0) {

            /* What about the shared pixmap type ? We need ZPixmap ! */
            if (XShmQueryVersion (rgi_display, &ShmMajor, &ShmMinor, &XShmPixmaps)) {
                if (XShmPixmaps == True && XShmPixmapFormat (rgi_display) == ZPixmap)
                    access_type = RGI_X11SHMPIXMAP_VIDEO;
                else
                    access_type = RGI_X11SHM_VIDEO;
            } else {
                fprintf (stderr, " [WARNING] *** Can't query SHM Extension version\n");
            }
        } else {
            fprintf (stderr, " [WARNING] *** SHM can't be used on remote display\n");
        }
    }
#endif /* HAVE_X11_EXTENSIONS_XSHM_H */

    return access_type;
}


/* Set the window application properties */
static void
rgi_set_wm_properties (rgi_viewport *v, uint32 flags)
{
    XSetWindowAttributes xsw;
    XSizeHints Hints;
    Atom wm_delete_atom;
    Atom wm_protocols_atom;
    Pixmap cursorpixmap;
    XColor color;

    if (flags & RGI_NORESIZE)
        Hints.flags = PSize | PMinSize | PMaxSize;      
    else
        Hints.flags = PSize;
      
    Hints.min_width = Hints.max_width = Hints.base_width = v -> screen -> w;
    Hints.min_height= Hints.max_height= Hints.base_height = v -> screen -> h;
    XSetWMNormalHints (rgi_display, v -> window, &Hints);

    xsw.border_pixel = WhitePixel (rgi_display, rgi_screennum);
    xsw.background_pixel = BlackPixel (rgi_display, rgi_screennum);
    xsw.win_gravity = NorthWestGravity;
    XChangeWindowAttributes (rgi_display, v -> window,
                             CWBackPixel | CWBorderPixel | CWWinGravity,
                             &xsw);

    /* Allow the window manager to delete our window (ICCM) */
    wm_protocols_atom = XInternAtom (rgi_display, "WM_PROTOCOLS", False);
    wm_delete_atom = XInternAtom (rgi_display, "WM_DELETE_WINDOW", False);
    if (wm_protocols_atom != (Atom) None && wm_delete_atom != (Atom) None)
        XSetWMProtocols (rgi_display, v -> window, &wm_delete_atom, 1);
  
    XClearWindow (rgi_display, v -> window);
  
    /* We can manage keyboard && mouse */
    XSelectInput (rgi_display, v -> window, (ButtonPressMask |
                                             KeyPressMask |
                                             StructureNotifyMask));
    v -> gc = XCreateGC (rgi_display, v -> window, 0, NULL);
    XMapRaised (rgi_display, v -> window);

    /* Hide the mouse pointer ? */
    if (flags & RGI_HIDEPTR) {
        /* Set the cursor to a 1x1 transparent pixmap */
        cursorpixmap = XCreatePixmap (rgi_display, v -> window, 1, 1, 1);
        if (cursorpixmap) {
            color.pixel = WhitePixel (rgi_display, rgi_screennum);  
            v -> cursor = XCreatePixmapCursor (rgi_display, cursorpixmap,
                                               cursorpixmap, &color, &color, 0, 0);
            XFreePixmap (rgi_display, cursorpixmap);
            XDefineCursor (rgi_display, v -> window, v -> cursor);
        }
    }  
}

rgi_viewport *
rgi_sys_set_videomode (int width, int height, int bpp, uint32 flags)
{
    rgi_viewport *v;
    rgi_pixel_format *pf;

    /* create the viewport we are going to initialize */
    v = rgi_sys_new_viewport ();

    /* create the screen surface which is the *REAL* screen start address */
    v -> screen = rgi_create_surface (width, height, rgi_format.BitsPerPixel,
                                      rgi_format.Rmask, rgi_format.Gmask,
                                      rgi_format.Bmask, rgi_format.Amask, flags);

    /* Now we start the best video access detection */
    v -> VideoAccessType = rgi_detect_access_type (v);

    /* If we don't have DGA, we create the window */
    if (v -> VideoAccessType != RGI_X11DGA_VIDEO) {
        v -> window = XCreateWindow (rgi_display, rgi_rootwindow, 0, 0,
                                     v -> screen -> w, v -> screen -> h, 0,
                                     rgi_depth, InputOutput, rgi_visual, 0, NULL);

        if (!v -> window)
            rgi_error ("Couldn't create Window !");
    
        rgi_set_wm_properties (v, flags);

#ifdef HAVE_X11_EXTENSIONS_XF86VMODE_H
        {
            int x, y, dummy;
            int best_mode;
            XEvent event;
            XF86VidModeModeLine current_mode;
            int current_dotclock;

            XMapRaised (rgi_display, v -> window);
            XSync (rgi_display, True);

            do {
                XMaskEvent(rgi_display, StructureNotifyMask, &event);
            } while (event.type != MapNotify || event.xmap.event != v -> window);

            best_mode = rgi_select_best_mode (v -> screen ->w, v -> screen ->h);
            rgi_switch_to_mode (best_mode);

            XF86VidModeGetModeLine (rgi_display, rgi_screennum,
                                    &current_dotclock, &current_mode);

            if (current_mode.hdisplay != saved_mode.hdisplay ||
                current_mode.vdisplay != saved_mode.vdisplay) {

                XMoveWindow (rgi_display, v -> window, 0, 0);
                XRaiseWindow (rgi_display, v -> window);

                rgi_move_cursor_to (0, 0);
                rgi_set_viewport_position (0, 0);
      
                XGetGeometry (rgi_display, v -> window, &rgi_rootwindow, &x, &y,
                              &dummy, &dummy, &dummy, &dummy);
                XMoveWindow (rgi_display, v -> window, -x, -y);
	
                XSync (rgi_display, True);
            }
        }
#endif /* HAVE_X11_EXTENSIONS_XF86VMODE_H */
    }

    /* initialize the alternate buffer */
    pf = rgi_create_rgb_pixel_format (bpp);
    v -> buffer = rgi_create_surface (width, height, bpp,
                                      pf -> Rmask, pf -> Gmask,
                                      pf -> Bmask, pf -> Amask, flags);
    rgi_free_pixel_format (pf);

    if (rgi_compare_pixel_format (v -> screen -> format,
                                  v -> buffer -> format) == 0)
    {
        rgi_free_surface (v -> buffer);
        v -> buffer = v -> screen;
    }
  
    switch (v -> VideoAccessType)
    {
    case RGI_X11_VIDEO:
        v -> ximage = XCreateImage (rgi_display, rgi_visual, rgi_depth,
                                    /*v -> screen -> format -> BitsPerPixel,*/
                                    ZPixmap, 0, (uint8 *) v -> screen -> pixels,
                                    v -> screen -> w, v -> screen -> h,
                                    BitmapPad (rgi_display),
                                    (v -> screen -> w
                                     * v -> screen -> format -> BytesPerPixel));
        if (!v -> ximage)
            rgi_error ("Couldn't create XImage !\n");
        break;

#ifdef HAVE_X11_EXTENSIONS_XSHM_H
    case RGI_X11SHM_VIDEO:
        if (!XShmQueryVersion (rgi_display, &ShmMajor, &ShmMinor, &XShmPixmaps))
            rgi_error ("Shm extension not available !\n");

        v -> shmseginfo = (XShmSegmentInfo *) malloc (sizeof (XShmSegmentInfo));
        assert (v -> shmseginfo);

        memset (v -> shmseginfo, 0, sizeof (XShmSegmentInfo));

        v -> ximage = XShmCreateImage (rgi_display, rgi_visual,
                                       rgi_depth, ZPixmap, NULL, v -> shmseginfo,
                                       v -> screen -> w, v -> screen -> h);
        if (!v -> ximage)
            rgi_error ("Can't create Shm XImage !\n");
      
        v -> shmseginfo -> shmid = shmget (IPC_PRIVATE,
                                           v -> ximage -> bytes_per_line
                                           * v -> ximage -> height,
                                           IPC_CREAT | 0777);
        if (v -> shmseginfo -> shmid < 0)
            rgi_error ("Can't obtain shm identifier !\n");
      
        if (v -> screen -> pixels)
            free(v -> screen -> pixels);

        v -> screen -> pixels =
            (void *) (v -> ximage -> data = v -> shmseginfo -> shmaddr
                      = shmat (v -> shmseginfo -> shmid, NULL, 0));
        if (!v -> screen -> pixels)
            rgi_error ("Can't attach shm\n");

        v -> shmseginfo -> readOnly = False;
      
        if (XShmAttach (rgi_display, v -> shmseginfo) == 0)
            rgi_error ("Can't attach shm segment to display\n");

        break;

    case RGI_X11SHMPIXMAP_VIDEO:
        if (!XShmQueryVersion (rgi_display, &ShmMajor, &ShmMinor, &XShmPixmaps))
            rgi_error ("Shm extension not available !\n");

        v -> shmseginfo = (XShmSegmentInfo *) malloc (sizeof (XShmSegmentInfo));
        assert (v -> shmseginfo);

        memset (v -> shmseginfo, 0, sizeof (XShmSegmentInfo));

        v -> shmseginfo -> shmid = shmget (IPC_PRIVATE, v -> screen -> realsize,
                                           IPC_CREAT | 0777);
        if (v -> shmseginfo -> shmid < 0)
            rgi_error ("Can't obtain shm identifier !\n");

        if (v -> screen -> pixels)
            free(v -> screen -> pixels);

        v -> screen -> pixels =
            (void *) (v -> shmseginfo -> shmaddr
                      = shmat (v -> shmseginfo -> shmid, 0, 0));
        if (!v -> screen -> pixels)
            rgi_error ("Can't attach shm !\n");

        v -> shmseginfo -> readOnly = False;
        if (!XShmAttach (rgi_display, v -> shmseginfo))
            rgi_error ("Can't attach shm segment to display\n");

        v -> pixmap = XShmCreatePixmap (rgi_display, v -> window,
                                        (uint8 *) v -> screen -> pixels,
                                        v -> shmseginfo, v -> screen -> w,
                                        v -> screen -> h, rgi_depth);
        XSetWindowBackgroundPixmap (rgi_display, v -> window, v -> pixmap);
        break;
#endif /* HAVE_X11_EXTENSIONS_XSHM_H */

#ifdef HAVE_X11_EXTENSIONS_XF86DGA_H
    case RGI_X11DGA_VIDEO:	
        XGrabKeyboard (rgi_display, rgi_rootwindow, True,
                       GrabModeAsync, GrabModeAsync, CurrentTime);

        XGrabPointer (rgi_display, rgi_rootwindow, True,
                      PointerMotionMask | ButtonPressMask | ButtonReleaseMask,
                      GrabModeAsync, GrabModeAsync, None,  None, CurrentTime);
      
        XF86DGADirectVideo (rgi_display, rgi_screennum, XF86DGADirectGraphics |
                            XF86DGADirectMouse | XF86DGADirectKeyb);

#ifdef HAVE_X11_EXTENSIONS_XF86VMODE_H
        {
            int best_mode;

            best_mode = rgi_select_best_mode (v -> screen -> w, v -> screen -> h);
            rgi_switch_to_mode (best_mode);
            rgi_set_viewport_position (0, 0);
        }
#endif /* HAVE_X11_EXTENSIONS_XF86VMODE_H */

        XF86DGASetViewPort (rgi_display, rgi_screennum, 0, 0);
        break;
#endif /* HAVE_X11_EXTENSIONS_XF86DGA_H */

    default:
        rgi_error ("Bad access type !\n");
    }

    return v;
}


void
rgi_sys_set_appname (rgi_viewport *v)
{
    XClassHint *classhints;

    if (!v -> window)
        return ;

    XStoreName (rgi_display, v -> window, rgi_app_name);

    classhints = XAllocClassHint();
    if (classhints)
    {
        classhints -> res_name = rgi_app_name;
        classhints -> res_class = rgi_app_name;
        XSetClassHint (rgi_display, v -> window, classhints);
        XFree (classhints);
    } 
}


void
rgi_sys_update_video (rgi_viewport *v)
{
    switch (v -> VideoAccessType) {
    case RGI_X11_VIDEO:
        XPutImage (rgi_display, v -> window, v -> gc, v -> ximage, 0, 0, 0, 0,
                   v -> screen -> w, v -> screen -> h);
        break;

#ifdef HAVE_X11_EXTENSIONS_XSHM_H
    case RGI_X11SHM_VIDEO:
        XShmPutImage (rgi_display, v -> window, v -> gc, v -> ximage,
                      0, 0, 0, 0, v -> screen -> w, v -> screen -> h, False);
        break;

    case RGI_X11SHMPIXMAP_VIDEO:
        XClearWindow (rgi_display, v -> window);
        break;
#endif /* HAVE_X11_EXTENSIONS_XSHM_H */

#ifdef HAVE_X11_EXTENSIONS_XF86DGA_H
    case RGI_X11DGA_VIDEO:
    {
        int y;

        XF86DGASetVidPage (rgi_display, rgi_screennum, 0);
        for (y = 0; y < v -> screen -> realh; y++)
        {
            memcpy ((uint8 *) (v -> DGAaddr + y * v -> DGAwidthsize),
                    (uint8 *) (v -> screen -> pixels +
                               (y * v -> screen -> realw)),
                    v -> screen -> realw);
        }
    }
    break;
#endif /* HAVE_X11_EXTENSIONS_XF86DGA_H */
    }
  
    /* DEFAULT SHOULD BE SYNCHRONOUS BLIT WITH XSYNC() */
    /*
      if ( SDL_VideoSurface->flags & SDL_ASYNCBLIT ) {
      XFlush(GFX_Display);
      ++blit_queued;
      } else {
    */
    /* needed for fast machines with low cpu consumption routines */
    /*
      XSync(GFX_Display, False);
      }
    */
    // only for SHMs ?
    XSync (rgi_display, False);
}


void
rgi_sys_set_palette (rgi_palette *p, rgi_viewport *v)
{
    uint32 *pixels;
    XColor color;
    int i;

    if (v -> colormap != None)
        XFreeColormap (rgi_display, v -> colormap);

    if (v -> palette != NULL)
        free (v -> palette);

    v -> palette = (uint32 *) malloc (sizeof (uint32) * p -> ncolors);
    assert (v -> palette);

    pixels = v -> palette;

    v -> colormap = XCreateColormap (rgi_display, rgi_rootwindow,
                                     rgi_visual, AllocNone);

    if (!XAllocColorCells (rgi_display, v -> colormap, False, 0, 0,
                           pixels, p -> ncolors))
        rgi_error ("XAllocColorCells: no more free color cells\n");
      
    color.flags = DoRed | DoGreen | DoBlue;
    for (i = 0; i < p -> ncolors; i++)
    {
        color.red = p -> colors[i] -> r << 8;
        color.green = p -> colors[i] -> g << 8;
        color.blue = p -> colors[i] -> b << 8;
        color.pixel = *pixels++;
        XStoreColor (rgi_display, v -> colormap, &color);
    }
      
    if (v -> VideoAccessType != RGI_X11DGA_VIDEO)
        XSetWindowColormap (rgi_display, v -> window, v -> colormap);
#ifdef HAVE_X11_EXTENSIONS_XF86DGA_H
    else
        XF86DGAInstallColormap (rgi_display, rgi_screennum, v -> colormap);
#endif /* HAVE_X11_EXTENSIONS_XF86DGA_H */
}


void
rgi_sys_write_video_infos (rgi_viewport *v)
{
    printf (" Type of Graphic Access : ");
    switch (v -> VideoAccessType)
    {
    case RGI_X11_VIDEO:
        printf ("Standard Xlib calls\n");
        break;
    case RGI_X11SHM_VIDEO:
        printf ("Shared memory extension %d.%d\n", ShmMajor, ShmMinor);
        break;
    case RGI_X11SHMPIXMAP_VIDEO:
        printf ("Shared memory pixmap %d.%d\n", ShmMajor, ShmMinor);
        break;
    case RGI_X11DGA_VIDEO:
        printf ("Direct Graphic Access extension %d.%d\n", DGAMajor, DGAMinor);
        break;
    default:
        printf ("unknown!\n");
    }
    printf (" Server Vendor          : ");
    printf ("%s\n", ServerVendor(rgi_display));
}
