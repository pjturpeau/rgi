#include <stdlib.h>
#include <stdio.h>

#include "rgi.h"

#define ULI unsigned long

#define OFFSET0 \
printf("%%define VIEWPORT_BUFFER_SURFACE	0x%lX\n", (ULI)&((struct viewport_struct *)0) -> buffer)
#define OFFSET00 \
printf("%%define VIEWPORT_SCREEN_SURFACE	0x%lX\n", (ULI)&((struct viewport_struct *)0) -> screen)
#define OFFSET1 \
printf("%%define SURFACE_PIXELS		0x%lX\n", (ULI)&((struct surface_struct *)0) -> pixels)
#define OFFSET2 \
printf("%%define SURFACE_WIDTH		0x%lX\n", (ULI)&((struct surface_struct *)0) -> w)
#define OFFSET3 \
printf("%%define SURFACE_HEIGHT		0x%lX\n", (ULI)&((struct surface_struct *)0) -> h)
#define OFFSET4 \
printf("%%define SURFACE_SIZE		0x%lX\n", (ULI)&((struct surface_struct *)0)->size)
#define OFFSET5 \
printf("%%define SURFACE_FORMAT		0x%lX\n", (ULI)&((struct surface_struct *)0)->format)
#define OFFSET6 \
printf("%%define PIXELFORMAT_KEYS16BITS	0x%lX\n", (ULI)&((struct pixel_struct *)0)->keys16bits)
#define OFFSET7 \
printf("%%define PIXELFORMAT_KEYS32BITS	0x%lX\n", (ULI)&((struct pixel_struct *)0)->keys32bits)


int
main(void)
{
  printf("%%ifndef RGI_NASM_OFFSETS_H\n%%define RGI_NASM_OFFSETS_H\n\n");
  OFFSET0;
  OFFSET00;
  OFFSET1;
  OFFSET2;
  OFFSET3;
  OFFSET4;
  OFFSET5;
  OFFSET6;
  OFFSET7;  
  printf("\n%%endif\n");
  return 0;
}
