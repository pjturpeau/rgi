#include <stdlib.h>

#ifdef HAVE_CONFIG_H
#include "rgi_config.h"
#endif /* HAVE_CONFIG_H */

#ifdef HAVE_SYS_TIME_H
#include <stdio.h>
#include <sys/time.h>
#endif /* HAVE_SYS_TIME_H */

#include <time.h>
#include <unistd.h>

#include "rgi_timer.h"

struct timeval fps_start_time;
struct timeval fps_end_time;
struct timeval fps_current_time;
struct timeval fps_elapsed_time;

int fps_max;
int fps_max_time;

int frames;			/* the frame counter */

void
rgi_set_max_fps (int maxfps)
{
    fps_max = maxfps;
    fps_max_time = 1000000 / fps_max;
}

/* Call it before the beginning of the main loop */
void
rgi_init_timer ()
{
    timerclear (&fps_elapsed_time);
    frames = 1;
    rgi_set_max_fps (DEFAULT_MAX_FPS);
    gettimeofday (&fps_start_time, NULL);
    fps_current_time = fps_start_time;
}

void
rgi_update_timer ()
{
    frames++;
}

void
rgi_end_timer ()
{  
    gettimeofday (&fps_end_time, NULL);
}

float
rgi_get_fps ()
{
    long microseconds;
  
    if (fps_end_time.tv_usec >= fps_start_time.tv_usec)
        microseconds = ((fps_end_time.tv_sec - fps_start_time.tv_sec) * 1000000
                        + fps_end_time.tv_usec - fps_start_time.tv_usec);
    else
        microseconds = ((fps_end_time.tv_sec
                         - fps_start_time.tv_sec - 1 ) * 1000000
                        + 1000000 + fps_end_time.tv_usec - fps_start_time.tv_usec);

    return (1000000.0 * frames) / microseconds;
}


int
rgi_get_timer_status ()
{
    gettimeofday (&fps_current_time, NULL);
    fps_elapsed_time.tv_sec = fps_current_time.tv_sec - fps_start_time.tv_sec;

    if (fps_current_time.tv_usec >= fps_start_time.tv_usec)
        fps_elapsed_time.tv_usec = (fps_current_time.tv_usec
                                    - fps_start_time.tv_usec);
    else
    {
        fps_elapsed_time.tv_usec = (1000000 + fps_current_time.tv_usec
                                    - fps_start_time.tv_usec);
        fps_elapsed_time.tv_sec--;
    }
  
    return (fps_elapsed_time.tv_sec * 1000000 + fps_elapsed_time.tv_usec);
}

/* ========================================================================= */

/* not really usefull in a demo, even if we are in a multi-tasking
   operating system, let's get all the CPU ! */
void
rgi_timer_sleep ()
{
    int frametime;

    frametime = fps_max_time - rgi_get_timer_status () / frames;
    if (frametime > 0)
        usleep (frametime);
}
