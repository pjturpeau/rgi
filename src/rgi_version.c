#include <stdlib.h>
#include <stdio.h>

#include "rvlasc.h"
#include "rgi_version.h"

void
rgi_banner (char *n)
{
    printf ("%s\n", rvlasc);
    printf ("%s %d.%d.%d (c)opyleft 2000-2004 by %s\n",
            n, MAJOR, MINOR, PATCH, AUTHOR);
}
