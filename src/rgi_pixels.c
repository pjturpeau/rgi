#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#ifdef HAVE_CONFIG_H
#include "rgi_config.h"
#endif /* HAVE_CONFIG_H */

#include "rgi_types.h"
#include "rgi_colors.h"
#include "rgi_pixels.h"

void
rgi_free_pixel_format (rgi_pixel_format *pf)
{
    if (pf -> palette)
        rgi_free_palette (pf -> palette);
    free (pf);
}

rgi_pixel_format *
rgi_new_pixel_format ()
{
    rgi_pixel_format *p
        = (rgi_pixel_format *) malloc (sizeof (rgi_pixel_format));
    assert (p);
  
    return p;
}

rgi_pixel_format *
rgi_create_pixel_format (int bpp, uint32 Rmask, uint32 Gmask, uint32 Bmask,
                         uint32 Amask)
{
    rgi_pixel_format *p;
    uint32 mask;

    p = rgi_new_pixel_format ();

    p -> BitsPerPixel = bpp;
    p -> BytesPerPixel = (bpp + 7) / 8;

    switch (bpp)
    {
    case 1:
        p -> palette = rgi_create_palette (2);
      
        p -> palette -> colors[0] -> r = 0x00;
        p -> palette -> colors[0] -> g = 0x00;
        p -> palette -> colors[0] -> b = 0x00;
        p -> palette -> colors[0] -> a = 0x00;

        p -> palette -> colors[1] -> r = 0xFF;
        p -> palette -> colors[1] -> g = 0xFF;
        p -> palette -> colors[1] -> b = 0xFF;
        p -> palette -> colors[1] -> a = 0xFF;

        p -> Rloss = p -> Gloss = p -> Bloss = p -> Aloss = 8;
        p -> Rshift = p -> Gshift = p -> Bshift = p -> Ashift = 0;
        p -> Rmask = p -> Gmask = p -> Bmask = p -> Amask = 0;
        break;
    case 4:
        p -> palette = rgi_create_palette (16);

        p -> Rloss = p -> Gloss = p -> Bloss = p -> Aloss = 8;
        p -> Rshift = p -> Gshift = p -> Bshift = p -> Ashift = 0;
        p -> Rmask = p -> Gmask = p -> Bmask = p -> Amask = 0;
        break;
    case 8:
        p -> palette = rgi_create_palette (256);

        p -> Rloss = p -> Gloss = p -> Bloss = p -> Aloss = 8;
        p -> Rshift = p -> Gshift = p -> Bshift = p -> Ashift = 0;
        p -> Rmask = p -> Gmask = p -> Bmask = p -> Amask = 0;
        break;
    default:
        p -> palette = NULL;

        p -> Rmask = Rmask;
        p -> Gmask = Gmask;
        p -> Bmask = Bmask;
        p -> Amask = Amask;

        p -> Rloss = 8;
        p -> Rshift = 0;
        if (Rmask)
        {
            for (mask = Rmask; !(mask & 0x01); mask >>= 1)
                p -> Rshift++;
            for ( ; (mask & 0x01); mask >>= 1)
                p -> Rloss--;
        }

        p -> Gloss = 8;
        p -> Gshift = 0;
        if (Gmask)
        {
            for (mask = Gmask; !(mask & 0x01); mask >>= 1)
                p -> Gshift++;
            for ( ; (mask & 0x01); mask >>= 1)
                p -> Gloss--;
        }

        p -> Bloss = 8;
        p -> Bshift = 0;
        if (Bmask)
        {
            for (mask = Bmask; !(mask & 0x01); mask >>= 1)
                p -> Bshift++;
            for ( ; (mask & 0x01); mask >>= 1)
                p -> Bloss--;
        }

        p -> Aloss = 8;
        p -> Ashift = 0;
        if (Amask)
        {
            for (mask = Amask; !(mask & 0x01); mask >>= 1)
                p -> Ashift++;
            for ( ; (mask & 0x01); mask >>= 1)
                p -> Aloss--;
        }

    }

    p -> Rcomp = 8 - p -> Rloss;
    p -> Gcomp = 8 - p -> Gloss;
    p -> Bcomp = 8 - p -> Bloss;
    p -> Acomp = 8 - p -> Aloss;

    return p;
}

rgi_pixel_format *
rgi_create_rgb_pixel_format (int bpp)
{
    rgi_pixel_format *pf = NULL;

    switch (bpp)
    {
    case 1:
    case 4:
    case 8:  
        pf = rgi_create_pixel_format (bpp, 0, 0, 0, 0);
        break;
    case 15:
        pf = rgi_create_pixel_format (bpp, 0x7C00 , 0x3E0, 0x1F, 0);
        break;
    case 16:
        pf = rgi_create_pixel_format (bpp, 0xF800 , 0x7E0, 0x1F, 0);
        break;
    case 24:
        pf = rgi_create_pixel_format (bpp, 0xFF0000 , 0x00FF00, 0x0000FF, 0);
        break;
    case 32:
        pf = rgi_create_pixel_format (bpp, 0x00FF0000 , 0x0000FF00, 0x000000FF,
                                      0);
        break;
    }
    return pf;
}

void
rgi_write_pixel_format (rgi_pixel_format *pf)
{
    printf ("Palette: %p - RGBA: %d%d%d%d\n", pf -> palette, pf -> Rcomp,
            pf -> Gcomp, pf -> Bcomp, pf -> Acomp);
    printf ("Rmask: %032lX\n", pf -> Rmask);
    printf ("Gmask: %032lX\n", pf -> Gmask);
    printf ("Bmask: %032lX\n", pf -> Bmask);
    printf ("Amask: %032lX\n", pf -> Amask);

    printf ("Rshift: %d\n", pf -> Rshift);
    printf ("Gshift: %d\n", pf -> Gshift);
    printf ("Bshift: %d\n", pf -> Bshift);
    printf ("Ashift: %d\n", pf -> Ashift);

    printf ("Rloss: %d\n", pf -> Rloss);
    printf ("Gloss: %d\n", pf -> Gloss);
    printf ("Bloss: %d\n", pf -> Bloss);
    printf ("Aloss: %d\n", pf -> Aloss);
}

int
rgi_compare_pixel_format (rgi_pixel_format *pf1, rgi_pixel_format *pf2)
{
    return ((pf1 -> BitsPerPixel - pf2 -> BitsPerPixel)
            + (pf1 -> BytesPerPixel - pf2 -> BytesPerPixel)
            + (pf1 -> Rmask - pf2 -> Rmask) + (pf1 -> Gmask - pf2 -> Gmask)
            + (pf1 -> Bmask - pf2 -> Bmask) + (pf1 -> Amask - pf2 -> Amask));  
}

uint32
rgi_build_color_key (rgi_pixel_format *dst, uint8 r, uint8 g, uint8 b)
{
    uint8 red, green, blue;

    red = (r & 0xFF) >> dst -> Rloss;
    green = (g & 0xFF) >> dst -> Gloss;
    blue = (b & 0xFF) >> dst -> Bloss;

    return ((red << dst -> Rshift)
            + (green << dst -> Gshift)
            + (blue << dst -> Bshift));
}

void
rgi_get_color_component (rgi_pixel_format *src, uint32 colorkey,
                         uint8 *r, uint8 *g, uint8 *b)
{
    *r = ((colorkey & src -> Rmask) >> src -> Rshift) << src -> Rloss;
    *g = ((colorkey & src -> Gmask) >> src -> Gshift) << src -> Gloss;
    *b = ((colorkey & src -> Bmask) >> src -> Bshift) << src -> Bloss;
}

void
rgi_pixel_format_set_bits_per_pixel (rgi_pixel_format *pf, int bpp)
{
    pf -> BitsPerPixel = bpp;
    pf -> BytesPerPixel = (bpp + 7) / 8;
}
