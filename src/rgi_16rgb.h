#ifndef RGI_16RGB_H
#define RGI_16RGB_H

#ifdef __USE_X86_ASSEMBLER__
//extern void TMS_Convert_x86_16RGB565_32RGB888(TMS_Surface *s_dst,
//					      TMS_Surface *s_src);

//extern void TMS_Convert_x86_16RGB565_8INDEXED(TMS_Surface *s_dst,
//					      TMS_Surface *s_src);
#endif /* __USE_X86_ASSEMBLER__ */

extern void rgi_convert_LE_C_16RGB565_32RGB888(rgi_surface *s_dst,
					       rgi_surface *s_src);

extern void rgi_convert_LE_C_16RGB565_8INDEXED(rgi_surface *s_dst,
					       rgi_surface *s_src);

#endif /* RGI_16RGB_H */
