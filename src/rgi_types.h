#ifndef RGI_TYPES_H
#define RGI_TYPES_H

#define STATIC_STRING	1024

#define TRUE  (0 == 0)
#define FALSE (0 != 0)

#ifndef Bool
typedef enum {False = TRUE, True = FALSE} Bool;
#endif /* Bool */

typedef unsigned char	uint8;
typedef unsigned short	uint16;
typedef unsigned long	uint32;

typedef char  sint8;
typedef short sint16;
typedef long  sint32;

typedef float float32;

#endif /* RGI_TYPES_H */
