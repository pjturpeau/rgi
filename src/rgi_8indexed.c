#include <stdlib.h>

#include "rgi_types.h"
#include "rgi_colors.h"
#include "rgi_pixels.h"
#include "rgi_surface.h"
#include "rgi_8indexed.h"

/*
 * Little Endian 8->16 convertion
 */
void
rgi_convert_LE_C_8INDEXED_16RGB565(rgi_surface *s_dst, rgi_surface *s_src)
{
  int i;
  uint32 o1 = 0;
  uint32 o2 = 0;

  uint32 *src = (uint32 *)s_src -> pixels;
  uint32 *dst = (uint32 *)s_dst -> pixels;
  uint32 pix, pix2;

  for (i = 0; i < s_src -> size >> 2; i++)
    {
      pix = src[o1++];
      
      pix2 = ((s_src -> format -> keys16bits[(pix >> 8) & 0xFF] << 16) +
	      s_src -> format -> keys16bits[pix & 0xFF]);
      dst[o2++] = pix2;

      pix2 = ((s_src -> format -> keys16bits[(pix >> 24)] << 16)
	      + s_src -> format -> keys16bits[(pix >> 16) & 0xFF]);
      dst[o2++] = pix2;
    }
}

void
rgi_convert_LE_C_8INDEXED_32RGB888(rgi_surface *s_dst, rgi_surface *s_src)
{
  int i;
  uint32 o1 = 0;
  uint32 o2 = 0;

  uint32 *src = (uint32 *)s_src -> pixels;
  uint32 *dst = (uint32 *)s_dst -> pixels;
  uint32 pix;

  for (i = 0; i < s_src -> size >> 2; i++)
    {
      pix = src[o1++];
      
      dst[o2++] = s_src -> format -> keys32bits[pix & 0xFF];
      dst[o2++] = s_src -> format -> keys32bits[(pix >> 8) & 0xFF];
      dst[o2++] = s_src -> format -> keys32bits[(pix >> 16) & 0xFF];
      dst[o2++] = s_src -> format -> keys32bits[(pix >> 24) & 0xFF];
    }
}

