#ifndef RGI_SURFACE_H
#define RGI_SURFACE_H

typedef struct surface_struct {
  
  void *pixels;
  int w, h;
  int size;

  int realw, realh;
  int realsize;

  uint32 flags;			/* unused */

  rgi_pixel_format *format;

  /* int locked; for threaded apps ... */

} rgi_surface;

extern rgi_surface *rgi_create_surface (int width, int height, int depth, 
					uint32 Rmask, uint32 Gmask,
					uint32 Bmask, uint32 Amask,
					uint32 flags);

extern void rgi_free_surface (rgi_surface *s);

extern void rgi_printf_surface (rgi_surface *s);

#endif /* RGI_SURFACE_H */
