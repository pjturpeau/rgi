#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "rgi.h"
#include "fractal.h"

#define MAPSHIFT       8
#define MAPSIZE        (1 << MAPSHIFT)
#define BYTE(a)        ((a) & (MAPSIZE - 1))
#define CLOUD_SIZE     100 // 1=small clouds. 100 is a good value.
#define INDEX(x,y)     ((BYTE(y) << MAPSHIFT) + BYTE(x))
#define RANDOM(range)  (rand() % (range))

uint8 *fractalmap = NULL;
uint8 *colormap = NULL;

void
smooth_map(uint8 *buf)		/* smooth 256x256x8bits texture */
{
  int x, y;
  
  for(x = 0; x < 256; x++)
    for(y = 0; y < 256; y++)
      {
	buf[INDEX(x, y)] = (buf[INDEX(x - 1, y - 1)]
			    + buf[INDEX(x - 1, y + 1)]
			    + buf[INDEX(x + 1, y - 1)]
			    + buf[INDEX(x + 1, y + 1)]
			    + buf[INDEX(x    , y - 1)]
			    + buf[INDEX(x    , y + 1)]
			    + buf[INDEX(x - 1, y)]
			    + buf[INDEX(x    , y)]
			    + buf[INDEX(x + 1, y)]) / 9;	
      }
}

int
RandPixel (int x, int y, int x1, int y1, int x2, int y2)
{
  int col;
 
  col = ((rand() % 200 - 100) * (abs(x - x1) + abs(y - y1)) / CLOUD_SIZE +
	 ((fractalmap[INDEX(x1, y1)] + fractalmap[INDEX(x2, y2)]) >> 1));

  if (col < 0)
    col = 0;
  if (col > 255)
    col = 254;

  fractalmap[INDEX(x, y)]= (uint8) col;
  
  return col;
}

int
RandPixel2 (int c, int s)
{
  int col = (rand() % 200 - 100) * (2 * s) / (CLOUD_SIZE) + c;
  if (col < 0)
    col = 0;
  if (col >= 255)
    col = 254;
  return col;
}

void
fractal (int x, int y, int size)
{
  int s, p;
  int xs, ys, x2, y2;
  int size2 = (size >> 1);
  
  if (size < 2)
    return;
  
  xs = x + size2;
  ys = y + size2;
  x2 = x + size;
  y2 = y + size;
  
  if ((p = fractalmap[INDEX(xs, y)]) == 255)
    p = RandPixel (xs, y, x, y, x2, y);

  if ((p = fractalmap[INDEX(x, ys)]) == 255)
    p = RandPixel (x, ys, x, y, x, y2);

  if ((p = fractalmap[INDEX(x2, ys)]) == 255)
    p = RandPixel (x2, ys, x2, y, x2, y2);

  if ((p = fractalmap[INDEX(xs, y2)]) == 255)
    p = RandPixel (xs, y2, x, y2, x2, y2);

  s = (fractalmap[INDEX(x, y)] + fractalmap[INDEX(x2, y2)] +
       fractalmap[INDEX(x, y2)] + fractalmap[INDEX(x2, y)]);
  fractalmap[INDEX(xs, ys)] = RandPixel2 ((s >> 2), size);

  fractal (x, y, size2);
  fractal (xs, y, size2);
  fractal (x, ys, size2);
  fractal (xs, ys, size2);
}

void
make_color_map()
{
  int x, y, temp;
  
  for (x = 0; x < 256; x++)
    for (y = 0; y < 256; y++)
      {
	temp = fractalmap[INDEX(x + 1, y)] - fractalmap[INDEX(x, y)];
	temp = ((temp * 330) >> 5) + 128;
	//temp=((temp*450)>>6)+128;
	colormap[INDEX(x, y)] = temp;
      }
}

void
create_map()
{
  srand ((unsigned) time (NULL));
  fractalmap = (uint8 *) malloc(65536 * sizeof (uint8));
  colormap = (uint8 *) malloc(65536 * sizeof (uint8));
  memset (fractalmap, 255, MAPSIZE * MAPSIZE);  

  fractalmap[0] = (rand() % 127 + 64);

  printf (" Calculating %dx%d fractal..", MAPSIZE, MAPSIZE);
  fractal (0, 0, 256);
  printf ("\n Smoothing map..");
  smooth_map (fractalmap);
  printf ("\n Creating color map..");
  make_color_map();
  printf ("\n Smoothing color map..");
  smooth_map (colormap);
  printf ("\n");
}

void
kill_map()
{
  if (fractalmap)
    {
      free (fractalmap);
      fractalmap = NULL;
    }
  if (colormap)
    {
      free (colormap);
      colormap = NULL;
    }
}
