#ifndef FRACTAL_H
#define FRACTAL_H

#include "rgi_types.h"

extern uint8 *fractalmap;
extern uint8 *colormap;

extern void create_map ();
extern void kill_map ();

#endif /* FRACTAL_H */
