
	bits 32

	%include "rgi_nasm_offsets.h"

	%define TARGETWIDTH 320
	%define TARGETHEIGHT 240
	%define TARGETCENTERX 160
	%define TARGETCENTERY 120

	extern	_bump_picture
	extern	_bump_pitch
	extern	_bump_yaw
	extern	_sin_table
	extern	_cos_table

	section .data

	_CX	dd 0
	_LY	dd 0
	_LX	dd 0

	section .text


%ifdef	__USE_X86_ASSEMBLER__

	;; argument is a pointer to a surface
	global	_bump
_bump:
	push	ebp
	mov	ebp, esp
	pushad

	mov	esi, [_bump_pitch]
	mov	edi, [_bump_yaw]
	shl	esi, 2
	shl	edi, 2
	mov	ebx, [esi + _cos_table]
	mov	edx, [edi + _sin_table]
	add	ebx, TARGETCENTERX
	add	edx, TARGETCENTERY	; edx = CY
	mov	[_CX], ebx
	
	mov	esi, [_bump_picture]
	mov	eax, [ebp + 8]		; we get the surface pointer
  	mov	edi, [eax + SURFACE_PIXELS]
	
	push	edi
	xor	eax, eax
	mov	ecx, TARGETWIDTH * TARGETHEIGHT / 2
	rep	stosd
	pop	edi
	
	add	esi, TARGETWIDTH	; source is a 8bit heights field buffer
	add	edi, TARGETWIDTH * 2	; 320*2 (we are in 16bpp)

	mov	ecx, 1
	mov	eax, ecx
	sub	eax, edx
	mov	[_LY], eax
.ForLoopY
	cmp	ecx, TARGETHEIGHT - 1
 	jl	.LL4
	jmp	.ExitForLoopY
.LL4
	push	ecx

	xor	eax, eax
	xor	ecx, ecx
	sub	eax, [_CX]
	mov	[_LX], eax
.ForLoopX
	cmp	ecx, TARGETWIDTH
	jl	.LL5
	jmp	.ExitForLoopX
.LL5
	push	ecx

	mov	eax, [_LX]
	mov	ecx, 127
	movzx	ebx, BYTE [esi + 1]
	sub	eax, ebx
	movzx	ebx, BYTE [esi - 1]
	add	ebx, eax
	jns	.L1
	neg	ebx
.L1
	sub	ecx, ebx
 	test	ecx, ecx
 	jle 	.skip
	
	mov	eax, [_LY]
	mov	edx, 127
	movzx	ebx, BYTE [esi + TARGETWIDTH]
	sub	eax, ebx
	movzx	ebx, BYTE [esi - TARGETWIDTH]
	add	ebx, eax
	jns	.L2
	neg	ebx
.L2
	sub	edx, ebx

 	test	edx, edx
 	jle 	.skip

	imul	ecx, edx
	shr	ecx, 6
	mov	eax, ecx
	mov	ebx, ecx

	shr	eax, 3
	shr	ebx, 3
	shr	ecx, 3

	shl	ax, 11
	shl	bx, 5
	add	ax, bx
	add	ax, cx

 	mov	[edi], ax
.skip

	pop	ecx
	inc	DWORD [_LX]
	add	edi, 2
	inc	esi
	inc	ecx
	jmp	.ForLoopX
.ExitForLoopX

	pop	ecx
	inc	DWORD [_LY]
	inc	ecx
	jmp	.ForLoopY

.ExitForLoopY:

	popad
	leave			; do the `pop ebp' itself
	ret
	 
%endif

