#ifndef BUMP_H
#define BUMP_H

extern uint8 *bump_picture;	/* Bump mapping texture */
extern int bump_pitch;		/* X Bump angle */
extern int bump_yaw;		/* Y Bump angle */

extern void bump_move (uint32 microseconds);
extern void bump (rgi_surface *surface);

#endif /* BUMP_H */
