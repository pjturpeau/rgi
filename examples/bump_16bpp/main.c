#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "rgi.h"
#include "bump.h"
#include "fractal.h"
#include "sincos.h"

int
main (int argc, char *argv[])
{
    rgi_viewport *v;
    int m, n;

    rgi_init (argc, argv, "RGI Bump");
  
    v = rgi_set_videomode (320, 240, 16,
                           RGI_FULLSCREEN | RGI_HIDEPTR | RGI_NORESIZE);


    printf ("\n");
    create_map();

    bump_picture = (uint8 *) malloc (v -> screen -> w * v -> screen -> h);
    assert (bump_picture);

    for (n = 0; n < 240; n++)
        memcpy ((uint8 *) &bump_picture[n * v->screen->w],
                (char *) &colormap[(n * 256) % 65536], 256);

    for (n = 0; n < 240; n++)
        for (m = 0; m < (v->screen->w - 256); m++)
            bump_picture[n * v->screen->w + 256 + m] = colormap[(n * 256 + m) % 65536];


    make_sin_cos (160, 100);

    rgi_init_timer ();

    while (rgi_loop)
    {
        rgi_sys_check_events (v);

        bump (v->buffer);
        bump_move (rgi_get_timer_status ());

        rgi_update_video (v);
        rgi_update_timer ();
        //rgi_timer_sleep ();
    }

    rgi_end_timer ();
    rgi_write_infos (v);
    rgi_close_videomode (v);
    rgi_quit ();

    kill_map ();

    return EXIT_SUCCESS;
}

