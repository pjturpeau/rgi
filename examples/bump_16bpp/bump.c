#include <stdlib.h>
#include <math.h>

#include "rgi.h"
#include "bump.h"
#include "fractal.h"
#include "sincos.h"

#define TARGETWIDTH 320//320
#define TARGETHEIGHT 240//200
#define TARGETWIDTH2  160//160
#define TARGETHEIGHT2 120//100

uint8 *bump_picture;
int bump_pitch = 0;
int bump_yaw = 0;

void
bump_move (uint32 microseconds)
{
    bump_pitch =  (96 * microseconds >> 18) & SINMASK;
    bump_yaw =  (64 * microseconds >> 18) & SINMASK; 
}

/*
 * 16bpp bump
 */
#ifndef __USE_X86_ASSEMBLER__
void
bump (rgi_surface *s)
{
    int cx,cy,x,y;
    int lx,ly;
    int r,g,b;
    int colx,coly,col;
    uint16 *scr = ((uint16 *) s -> pixels) + TARGETWIDTH;
    uint8 *bmp = bump_picture + TARGETWIDTH;

    cx = cos_table[bump_pitch] + TARGETWIDTH2;
    cy = sin_table[bump_yaw] + TARGETHEIGHT2;

    memset (scr, 0, TARGETWIDTH * TARGETHEIGHT);

    for (y = 1; y < (TARGETHEIGHT - 1); y++)
    {
        ly = y - cy;
        for (x = 0; x < TARGETWIDTH; x++)
        {
            lx = x - cx;
            colx = 127 - abs (lx - *(bmp + 1) + *(bmp - 1));
            coly = 127 - abs (ly - *(bmp + TARGETWIDTH) + *(bmp - TARGETWIDTH));

            if (colx > 0 && coly > 0)
            {
                col = (colx * coly) >> 6;
	    
                r = (col >> 3);
                g = ((col * 204) >> 10); /* (col / 5)=(col * (1024 / 5)) / 1024 */
                b = (col >> 3);

                *(scr) = (uint16) ((r << 11) + (g << 5) + b);	      
            }
            bmp++;
            scr++;
        }
    }
}
#endif /* __USE_X86_ASSEMBLER__ */
