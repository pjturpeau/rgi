#include <stdlib.h>
#include <math.h>

#include "sincos.h"

#define PI_ 3.14159265359

int sin_table[SINSIZE];
int cos_table[SINSIZE];

void
make_sin_cos (int amplitude_x, int amplitude_y)
{
    int i;

    for (i = 0 ; i < SINSIZE; i++)
    {
        cos_table[i] = (int) (amplitude_x * cos (PI_ * i / (SINSIZE / 2.0)));
        sin_table[i] = (int) (amplitude_y * sin (PI_ * i / (SINSIZE / 2.0)));
    }
}

