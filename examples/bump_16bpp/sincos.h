#ifndef SINCOS_H
#define SINCOS_H

#define SINSHIFT	10
#define SINSIZE		(1<<SINSHIFT)
#define SINMASK		(SINSIZE - 1)

extern int sin_table[SINSIZE];
extern int cos_table[SINSIZE];

extern void make_sin_cos (int amplitude_x,int amplitude_y);

#endif /* SINCOS_H */
