#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "rgi.h"

#define W            320
#define H            240
#define NAME         "rotozoom"

void refresh ();

void init_colormap();
void init_tables();
void init_texture();

#endif
