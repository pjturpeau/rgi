/*
 *
 * 8bits rotozoom stolen from OPTIMUM web site and adapted to RGI.
 *
 * optimum www -> http://optimum.sourceforge.net
 *
 */


#include "main.h"

#ifndef M_PI
#define	M_PI		3.14159265358979323846	/* pi */
#endif

#define PAL  "texture.pal"
#define DATA "texture.data"

static unsigned char texture [ 4 * 128*128 ];

#define ANGLE_MAX 1024
int teta = 0;
int h_cos [ANGLE_MAX];
int h_sin [ANGLE_MAX];

rgi_viewport *v;

int
main (int argc, char *argv[])
{
    rgi_init (argc, argv, NAME);

    v = rgi_set_videomode(W, H, 8, RGI_FULLSCREEN | RGI_HIDEPTR | RGI_NORESIZE);

    init_colormap();
    init_tables();
    init_texture();

    rgi_init_timer ();

    while ( rgi_loop )
    {
        rgi_sys_check_events (v);

        refresh(v -> buffer);

        rgi_update_video (v);
        rgi_update_timer ();
        rgi_timer_sleep ();
        
        teta = (rgi_get_timer_status() >> 13) % ANGLE_MAX;
    }

    rgi_end_timer ();
    rgi_write_infos (v);

    rgi_close_videomode (v);
    rgi_quit ();

    exit (0);
  
}

void
refresh (rgi_surface *surf)
{

    const int c = h_cos [teta];
    const int s = h_sin [teta];
  
    const int xi = -((W)/2) * c;
    const int yi =  (W/2) * s;
  
    const int xj = -(H/2) * s;
    const int yj = -((H)/2) * c;
  
    int i,j;
  
    int x,y;
    int xprime = xj;
    int yprime = yj;
    unsigned char * scrtmp = (uint8 *) surf -> pixels;


    for ( j=0 ; j<H ; j++ )
    {
        x = xprime + xi;
        xprime += s;

        y = yprime + yi;
        yprime += c;
      
        for ( i=0 ; i<W ; i++ )
        {
	  
            x += c;
            y -= s;
	  
            *(scrtmp++) = *( texture + ((x>>8)&255) + (y&(255<<8)) );

        }
      
    }      
      
}

void init_colormap() {

    int i;
    rgi_color colors[256];
  
    FILE *fichier;
  
    fichier = fopen ( PAL , "r" );
  
    for ( i=0 ; i<256 ; i++ )    
    {  
        fread ( &colors[i].r , 1 , 1 , fichier ); 
        fread ( &colors[i].g , 1 , 1 , fichier ); 
        fread ( &colors[i].b , 1 , 1 , fichier ); 
    }

    rgi_set_colors (v, colors, 0, 256);
  
    fclose ( fichier );
}

void init_tables() {
  
    int i;
    double h;
    double radian;
  
    for ( i=0 ; i < ANGLE_MAX ; i++ )
    {
      
        radian = 2 * i * M_PI / ANGLE_MAX;
      
        h = 1.1 + sin(radian);// 1.3 + sin (radian);      
      
        h_cos[i] = 256 * ( h * cos (radian) );
        h_sin[i] = 512 * ( h * sin (radian) );
	
    }

}

void init_texture() {
 
    /* Creation du damier */

    FILE *fichier;
    int i;

    fichier = fopen ( DATA , "r" );
  
    for ( i=0 ; i<128 ; i++ )
    {
        fread ( texture+256*i , 1 , 128 , fichier );
        memcpy ( 257*128+ texture+256*i , texture+256*i , 128 );
    }

    for ( i=0 ; i<128 ; i++ )
    {
        fread ( texture+256*i+128 , 1 , 128 , fichier );
        memcpy ( 256*128+ texture+256*i , texture+256*i+128 , 128 );
    }


    fclose ( fichier );
  
}
